import bottle
from bottle import route, run, static_file, post, request, response, get, HTTPResponse
import auth, json

def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, DELETE, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, key'
        if request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)
 
    return _enable_cors

# @route('/')
# @route('/<filepath:path>')
# @enable_cors
@route('/', method=['OPTIONS', 'GET'])
@route('/<filepath:path>', method=['OPTIONS', 'GET'])
@enable_cors
def server_static(filepath="index.html"): 
    # response.headers['Content-type'] = 'application/json'
    # if request.method == 'OPTIONS':
    #     return {}
    # else:
    return static_file(filepath, root='./')

@post('/api/login', method=['OPTIONS', 'POST']) 
@enable_cors
def do_login():
    password = request.params.password
    username = request.params.username
    print("Questa è la Username:{} e password: {}".format(username, password))
    result = auth.login_user(username, password)
    #result['token2']=result2
    print(result['status'])
    if(result['status'] == 201):
        result2 = result['result']
        print(result2)
        print('Token: ', result['token'])
        print('Rule: ', result2['token']['roles'])
        role = result2['token']['roles'][0]['name']
        result['role'] = role
        print("RUOLO", role)
        response.set_cookie('token', result['token'], path = '/')
        response.set_cookie('username', username, path = '/')
        response.set_cookie('role', role, path = '/')
        response.set_cookie('address', result['address'], path = '/') 

        """ result['token2']= result2
        response.set_cookie('token', result['headers']['X-Subject-Token'], path = '/')
        response.set_cookie('username', username, path = '/') """
        

    
    return result




run(host='0.0.0.0', port=8080, debug=True)

# if __name__ == '__main__':
#     from optparse import OptionParser
#     parser = OptionParser()
#     parser.add_option("--host", dest="host", default="localhost",
#                         help="hostname or ip address", metavar="host")
#     parser.add_option("--port", dest="port", default=8080,
#                         help="port number", metavar="port")
#     (options, args) = parser.parse_args()
#     run(app, host=options.host, port=int(options.port))


