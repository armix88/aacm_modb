var url_pat = "http://localhost:8001/getPatientList";
var url_doc = "http://localhost:8001/getDoctorList";
var url_icd = "http://localhost:8001/getIcdList";
var url_atc = "http://localhost:8001/getAtcList";
var url_submit = "http://localhost:8001/addTreatment";
var url_meeting = "http://localhost:8001/meetingResult";

let dropdownPatId = document.getElementById('inputPatId');
dropdownPatId.length = 0;

// let defaultOptionPatId = document.createElement('option');
// defaultOptionPatId.text = 'Select Patient';

// dropdownPatId.append(defaultOptionPatId);
// dropdownPatId.selectedIndex = 0;

const requestPatId = new XMLHttpRequest();
requestPatId.open('GET', url_pat, true);

requestPatId.onload = function() {
  if (requestPatId.status === 200) {
    const dataPatId = JSON.parse(requestPatId.responseText)['data'];
    let optionPatId;
    for (let i = 0; i < dataPatId.length; i++) {
        optionPatId = document.createElement('option');
        optionPatId.value = dataPatId[i].name + ' ' + dataPatId[i].surname;
        optionPatId.setAttribute("data-value", dataPatId[i].patid);
        dropdownPatId.append(optionPatId);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}

requestPatId.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

requestPatId.send();


let dropdownDocId = document.getElementById('inputDocId'); //TODO select logged in doctor as default
dropdownDocId.length = 0;

// let defaultOptionDocId = document.createElement('option');
// defaultOptionDocId.text = 'Select Doctor';

// dropdownDocId.add(defaultOptionDocId);
// dropdownDocId.selectedIndex = 0;

const requestDocId = new XMLHttpRequest();
requestDocId.open('GET', url_doc, true);

requestDocId.onload = function() {
  if (requestDocId.status === 200) {
    const data = JSON.parse(requestDocId.responseText)['data'];
    let option;
    for (let i = 0; i < data.length; i++) {
      option = document.createElement('option');
      option.value = data[i].name + ' ' + data[i].surname;
      option.setAttribute("data-value", data[i].docid);
      dropdownDocId.append(option);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}

requestDocId.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

requestDocId.send();

///////

let dropdownIcd = document.getElementById('inputIcd');
dropdownIcd.length = 0;

const requestIcd = new XMLHttpRequest();
requestIcd.open('GET', url_icd, true);

requestIcd.onload = function() {
  if (requestIcd.status === 200) {
    const data = JSON.parse(requestIcd.responseText)['data'];
    let option;
    for (let i = 0; i < data.length; i++) {
      option = document.createElement('option');
      option.value = data[i].code + ' ' + data[i].name;
      option.id = data[i].code;
      dropdownIcd.append(option);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}

requestIcd.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

requestIcd.send();

///////

let dropdownAtc = document.getElementById('inputAtc');
dropdownAtc.length = 0;

const requestAtc = new XMLHttpRequest();
requestAtc.open('GET', url_atc, true);

requestAtc.onload = function() {
  if (requestAtc.status === 200) {
    const data = JSON.parse(requestAtc.responseText)['data'];
    let option;
    for (let i = 0; i < data.length; i++) {
      option = document.createElement('option');
      option.value = data[i].code + ' ' + data[i].name;
      option.id = data[i].code;
      dropdownAtc.append(option);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}

requestAtc.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

requestAtc.send();

// Add tag
$("#icd").bind('change', function () {
  $("#tagsIcd").tagsinput('add', $(this).val().replace(',', ' '));
  $(this).val('');
});

// Add tag
$("#atc").bind('change', function () {
  $("#tagsAtc").tagsinput('add', $(this).val().replace(',', ' '));
  $(this).val('');
});
///////
// $("form#data").submit(function(e) {

//   var formData = new FormData(this); 
//   console.log(formData);
// });
function submitHere() {

    var form = new FormData();
      form.append('patid',document.querySelector("#inputPatId option[value='"+document.getElementById("patId").value+"']").dataset.value);
      form.append('docid',document.querySelector("#inputDocId option[value='"+document.getElementById("docId").value+"']").dataset.value);
      form.append('date',document.getElementById('inputDate').value);
      form.append('icd',$("#tagsIcd").val());
      form.append('atc',$("#tagsAtc").val());
      form.append('description',document.getElementById('inputDescription').value);
      form.append('file',$("#inputFile").prop('files')[0]);

    $.ajax({
        url: url_submit,
        type: 'POST',
        data: form,            
        processData: false,
        contentType: false,
        crossDomain: true,
        async:false,
        mimeType: "multipart/form-data",
        success: function(result) {
          console.log(result);
          result = $.parseJSON(result);
          console.log(result.status);
          console.log(result.message);
          if(result.status == 'Success'){
            var r = confirm("Registration successfully completed, press Ok to return to previous screen");
            // if (r == true) {
            //     window.location = 'home_doc_wbc.html'
            // }
          }
          if(result.status == 'Warning'){
            var r = confirm(result.message + "\nPress Ok to start a conference or Cancel to return");
            console.log("ret val");
            console.log(r);

            // treatid = result.data['treatid']
            // docid = result.data['docid']
            // patid = result.data['patid']

            meetingResult(r, result.data);
            if (r == true) {
              // window.location = 'home_doc_wbc.html'
              alert("Calling doctors");
            }
            if (r == false) {

              alert("Returning");
                // window.location = 'home_doc_wbc.html'
            }
          }
    //         var r = confirm("Registration successfully completed, press Ok to return to previous screen");
    //         if (r == true) {
    //             window.location = 'home_doc_wbc.html'
    //     }
    }
    });
    
}

function meetingResult(ret_val, data){

  console.log(data);
  var form = new FormData();
  form.append('result', ret_val);
  form.append('treatid', data['treatid']);
  form.append('patid', data['patid']);
  form.append('docid', data['docid']);

  $.ajax({
    url: url_meeting,
    type: 'POST',
    data: form,            
    processData: false,
    contentType: false,
    crossDomain: true,
    async:false,
    mimeType: "multipart/form-data",
    success: function(result) {
    }
  })
}

if (!session){
    window.location.replace('./index.html')
}
else {
    session = session.replace(/\\054/g, ',')
    //session = JSON.parse(JSON.parse(session))
    auth.token = session
    auth.role = getCookie('role')
    auth.username = getCookie('username')
    auth.username = getCookie('address')
}

function logout(){
    document.cookie = ''
}

