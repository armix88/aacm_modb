var auth = {
    token : undefined,
    role: undefined,
    address: undefined,
    username: undefined
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                      c = c.substring(1);
                    }
            if (c.indexOf(name) == 0) {
                      return c.substring(name.length, c.length);
                    }
          }
    return "";
}

var session = getCookie('token');

if (!session){
    
}
else {
    session = session.replace(/\\054/g, ',')
    //session = JSON.parse(JSON.parse(session))
    auth.token = session
    auth.role = getCookie('role')
    auth.username = getCookie('username')
    auth.address = getCookie('address')
}

function logout(){
    document.cookie = ''
}
