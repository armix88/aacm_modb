var url = "http://localhost:8001/getTreatments"
// var url_g = "http://90.147.188.235:8000/getAddress"
// var url_s = "http://90.147.188.235:8000/readFromNFC"

reload();

function reload() {
    var table = document.getElementById("dataTable");
    var tbody = table.getElementsByTagName('tbody')['mainTable'];
    // Empty table content before refresh
    $("#dataTable tbody").empty();

    // var tbodyInner = table.getElementsByTagName('tbody')['innerTable'];
    
    $.ajax({
        url: url,  
        type: 'GET',
        crossDomain: true,
        success: function(result) {
        // console.log(result['data'].length);
        var len = result['data'].length;
        for(var n = 0; n < len; n++){
            // console.log(result['data'][1]['analysis']);
            // console.log(result['data'][1]['analysis'].lenght);

            var tr = document.createElement('tr')
            
            // var tbodyInner = document.createElement('tbody')
            // tbodyInner.setAttribute("class", "collapse");  
            // tbodyInner.setAttribute("id", "innerTable" + n);

            // table.setAttribute("table-layout", "fixed");
            // table.setAttribute("width", "100px");

            // tbody.setAttribute("table-layout", "fixed");
            // tbody.setAttribute("width", "100px");


            // tr.setAttribute("table-layout", "fixed");
            // tr.setAttribute("width", "100px");

            // <!-- <tr class=""> -->
            // tr.setAttribute("class","clickable");
            // tr.setAttribute("class","row100 body");  
            // tr.setAttribute("data-toggle","collapse");
            // tr.setAttribute("data-target","#innerTable" + n);
            tr.setAttribute("aria-expanded","false");
            tr.setAttribute("aria-controls","innerTable" + n);

            var td0 = document.createElement('td');
            var td1 = document.createElement('td');
            var td2 = document.createElement('td');
            var td3 = document.createElement('td');
            var td4 = document.createElement('td');
            var td5 = document.createElement('td');
            var td6 = document.createElement('td');
            var td7 = document.createElement('td');
            var td8 = document.createElement('td');
            var td9 = document.createElement('td');
            var td10 = document.createElement('td');
            var td11 = document.createElement('td');

            td0.setAttribute("data-toggle","collapse");
            td0.setAttribute("data-target","#innerTable" + n);

            td0.appendChild(document.createTextNode(">"))
            td1.appendChild(document.createTextNode(result['data'][n]['doc']))
            td2.appendChild(document.createTextNode(result['data'][n]['name']))
            td3.appendChild(document.createTextNode(result['data'][n]['surname']))
            td4.appendChild(document.createTextNode(">"))
            td5.appendChild(document.createTextNode(">"))
            td4.setAttribute("data-toggle","collapse");
            td4.setAttribute("data-target","#innerTable" + n);
            td5.setAttribute("data-toggle","collapse");
            td5.setAttribute("data-target","#innerTable" + n);
            // td4.appendChild(document.createTextNode(result['data'][n]['icd']['icd']))
            // td5.appendChild(document.createTextNode(result['data'][n]['atc']['atc']))
            td6.appendChild(document.createTextNode(result['data'][n]['description']))


            var link = document.createElement('a');
            link.href = result['data'][n]['documentation'];
            link.target="_blank";
            var text = document.createTextNode('Go to documentation');
            
            link.appendChild(text);
            td7.appendChild(link);
            td8.appendChild(document.createTextNode(result['data'][n]['status']))
            
            analysis_value = "-";
            if(result['data'][n]['analysis'] != undefined){
                analysis_value = result['data'][n]['analysis']['description'] + " - " + result['data'][n]['analysis']['hash'];

                td9.appendChild(document.createTextNode(result['data'][n]['analysis']['description']))
                if(result['data'][n]['analysis']['hash'].length > 0) {

                    var link1 = document.createElement('a');
                    // link1.href = "https://ropsten.etherscan.io/tx/" + result['data'][n]['analysis']['hash'];
                    link1.href = result['data'][n]['analysis']['hash'];
                    link1.target="_blank";

                    var text1 = document.createTextNode('  Verify Hash on Blockchain');
                    
                    link1.appendChild(text1);
                    td9.appendChild(link1);
                }
            }
            if(result['data'][n]['meetingResult'] != undefined){
                analysis_value = result['data'][n]['meetingResult']['description'] + " - " + result['data'][n]['meetingResult']['hash'];

                td10.appendChild(document.createTextNode(result['data'][n]['meetingResult']['description']))
                if(result['data'][n]['meetingResult']['hash'].length > 0) {

                    var link1 = document.createElement('a');
                    // link1.href = "https://ropsten.etherscan.io/tx/" + result['data'][n]['meetingResult']['hash'];
                    link1.href = result['data'][n]['meetingResult']['hash'];
                    link1.target="_blank";

                    var text1 = document.createTextNode('  Verify Hash on Blockchain');
                    
                    link1.appendChild(text1);
                    td10.appendChild(link1);
                }
            }
            if(result['data'][n]['hash'] != undefined && result['data'][n]['hash'].length > 0) {

                var link2 = document.createElement('a');
                // link2.href = "https://ropsten.etherscan.io/tx/" + result['data'][n]['hash'];
                link2.href = result['data'][n]['hash'];
                link2.target="_blank";

                var text2 = document.createTextNode('  Verify Hash on Blockchain');
                
                link2.appendChild(text2);
                td11.appendChild(link2);
            }
            // td10.appendChild(document.createTextNode(result['data'][n]['hash']))

            tr.appendChild(td0);
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);
            tr.appendChild(td6);
            tr.appendChild(td7);
            tr.appendChild(td8);
            tr.appendChild(td9);
            tr.appendChild(td10);
            tr.appendChild(td11);

            tbody.appendChild(tr);

            icd_len = 0;
            atc_len = 0;

            if(result['data'][n]['icd'] != undefined)
                icd_len = result['data'][n]['icd'].length;

            if(result['data'][n]['atc'] != undefined)
                atc_len = result['data'][n]['atc'].length;

            if(icd_len > 0 || atc_len > 0) {
                if(icd_len >= atc_len) 
                    len_max = icd_len;
                
                else if(atc_len >= icd_len)
                    len_max = atc_len;
                
                for(var j = 0; j < len_max; j++) {
                    var tr1 = document.createElement('tr');

                    tr1.setAttribute("class", "collapse");  
                    tr1.setAttribute("id", "innerTable" + n);

                    var tdInner0 = document.createElement('td');
                    var tdInner1 = document.createElement('td');
                    var tdInner2 = document.createElement('td');
                    var tdInner3 = document.createElement('td');
                    var tdInner4 = document.createElement('td');
                    var tdInner5 = document.createElement('td');
                    var tdInner6 = document.createElement('td');
                    var tdInner7 = document.createElement('td');
                    var tdInner8 = document.createElement('td');
                    var tdInner9 = document.createElement('td');
                    var tdInner10 = document.createElement('td');
                    var tdInner11 = document.createElement('td');

                    icd_value = "-";
                    if(result['data'][n]['icd'] != undefined && result['data'][n]['icd'][j] != undefined){
                        icd_value = result['data'][n]['icd'][j]['icd'] + " - " + result['data'][n]['icd'][j]['icd_name'];
                    }

                    atc_value = "-";
                    if(result['data'][n]['atc'] != undefined && result['data'][n]['atc'][j] != undefined){
                        atc_value = result['data'][n]['atc'][j]['atc'] + " - " + result['data'][n]['atc'][j]['atc_name'];
                    }

                    tdInner4.appendChild(document.createTextNode(icd_value))
                    tdInner5.appendChild(document.createTextNode(atc_value))
                    // tdInner9.appendChild(document.createTextNode(analysis_value))

                    tr1.appendChild(tdInner0);
                    tr1.appendChild(tdInner1);
                    tr1.appendChild(tdInner2);
                    tr1.appendChild(tdInner3);
                    tr1.appendChild(tdInner4);
                    tr1.appendChild(tdInner5);
                    tr1.appendChild(tdInner6);
                    tr1.appendChild(tdInner7);
                    tr1.appendChild(tdInner8);
                    tr1.appendChild(tdInner9);
                    tr1.appendChild(tdInner10);

                    tbody.appendChild(tr1);
                }
            }
        }
        }   
    });
}


function redirectNewPatient(){
    window.location = 'home_doc_sub_patient.html'
    
}


function redirectNewPrescription(){
    window.location = 'home_doc_sub_prescription.html'
    
}


if (!session){
    window.location.replace('./index.html')
}
else {
    session = session.replace(/\\054/g, ',')
    //session = JSON.parse(JSON.parse(session))
    auth.token = session
    auth.role = getCookie('role')
    auth.username = getCookie('username')
    auth.username = getCookie('address')
}

function logout(){
    auth.token = undefined
    auth.role = undefined
    auth.username = undefined
    auth.username = undefined
    document.cookie = ''
    window.location = 'index.html';
}

// Add tag
$("#mainTable").on('click', 'tr', function() {
    // console.log($($this).find('td:first-child').text());
    var id = $(this).find("td:first-child").text();
    if(id == '>'){
        $(this).find("td:first-child").text("<");

        // $(this).find("td:first-child").attr("class", "fas fa-angle-right");
}
    else
        $(this).find("td:first-child").text(">");
        // $(this).find("td:first-child").attr("class", "fas fa-angle-left");
  });