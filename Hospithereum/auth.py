import requests
import json
from bson.json_util import dumps
import http.client

keystone_host = '90.147.188.235'
keystone_port = '5000'
ethereum_host = '90.147.188.235'
ethereum_port = '8000'

conn = http.client.HTTPConnection("90.147.188.235")


def login_user(username, password):
    try:
        post_data = {
            'auth': {
                'identity': {
                    'methods': ['password'],
                    'password': {
                        'user': {
                            'name': username,
                            'domain': {
                                'name': 'hospital'
                            },
                            'password': password
                        }
                    }
                },
                'scope': {
                    'project': {
                        'name': 'hospithereum',
                        'domain': {'name': 'hospital'}
                    }
                }
            }
        }
    except:
        return {'text': '{error: bad parameters found}', 'status': 400}

    request_path = 'http://{}:{}/v3/auth/tokens'.format(keystone_host, keystone_port)
    result =  requests.post(request_path, json = post_data)

    """
    request_path = 'http://{}:{}/getAddress'.format(ethereum_host, ethereum_port)
    result2 = requests.get(request_path)
    result2 = result2.json()
    address = ''
    print("Printa qui",result2)
    for data in result2['data']:
        if username == data['username']:
            address = data['address']
            continue
    """
    address = ''

    return {'result': result.json(), 'status': result.status_code, 'token': result.headers['X-Subject-Token'], 'address': address}

