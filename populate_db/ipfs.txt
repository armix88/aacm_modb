export ipfs_staging=/Users/armi/Hospital_multi/ipfs/staging
export ipfs_data=/Users/armi/Hospital_multi/ipfs/data

//Start a container running ipfs and expose ports 4001, 5001 and 8080:

docker run -d --name ipfs_host -v $ipfs_staging:/export -v $ipfs_data:/data/ipfs -p 4001:4001 -p 127.0.0.1:8080:8080 -p 127.0.0.1:5001:5001 ipfs/go-ipfs:latest


docker exec ipfs_host ipfs <.......>

echo "questo è un test" > $ipfs_staging/test.txt
cp -r <something> $ipfs_staging

docker exec ipfs_host ipfs add -r /export/test.txt

docker exec ipfs_host ipfs add -r /export/<something>

docker exec ipfs_host ipfs cat <hash_from_prev_cmd>

curl http://localhost:8080/ipfs/<hash_from_prev_cmd>


Peer identity? QmXgyuSYg2nUp23F7XKj2sjfSmgfjRfh9Q1fts2AkUJmat

curl -F "file=@/Users/armi/Hospital_multi/ipfs/staging/test.txt" "https://ipfs.infura.io:5001/api/v0/add?pin=false&quieter=true"  

curl "https://ipfs.infura.io:5001/api/v0/add?pin=false&quieter=true"     \
-X POST \
-H "Content-Type: multipart/form-data"     \
-F file=@"/Users/armi/Hospital_multi/ipfs/staging/test.txt"

{"Name":"test.txt","Hash":"QmcCym4QA2zq6VASRTUzYBBneaHPfLLTzXJJVaAStATK3p","Size":"26"}