import json
from pymongo import MongoClient


client = MongoClient('90.147.188.235', 8000)
db = client['patient']
indications = db['indications']

with open('/Users/armi/Downloads/indications.json') as fp:
    for f in fp:
        file_data = json.loads(f)

        print(file_data)
        indications.insert_one(file_data)  

client.close()