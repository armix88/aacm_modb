import datetime
import pymongo
from bson.objectid import ObjectId
from flask import Flask,render_template,jsonify,json,request
import random
import hashlib

client = pymongo.MongoClient('localhost:27017')
db = client.patient
patient = db.patient
doctor = db.doctor

def populate_patients():
    name = Name()
    surname = Surname()
    city = City()
    dob = Dob()

    # name = Icd9()

    if random.random() < 0.5:
        strSex = "M"
        strName = name.getMaleName()

    else:
        strSex = "F"
        strName = name.getFemaleName()
    
    strSurname = surname.getSurname()
    strCity = city.getCity()
    strDob = dob.getDob()

    strPatId = strName + strSurname + strSex + strDob + strCity

    strPatId = hashlib.sha1(strPatId.encode()).hexdigest()
            
    data = {"patid": strPatId
                        ,"name": strName
                        ,"surname": strSurname
                        ,"sex": strSex
                        ,"city": strCity
                        ,"dob": strDob
                        }
    print(data)
    patient.insert_one(data)



def populate_doctor():
    name = Name()
    surname = Surname()
    specialization = Specialization()
    role = Role()
    
    if random.random() < 0.5:
        strSex = "M"
        strName = name.getMaleName()

    else:
        strSex = "F"
        strName = name.getFemaleName()
    
    strSurname = surname.getSurname()

    strSpecialization = specialization.getSpecialization()
    
    strRole = role.getRole()

    strDocId = strName + strSurname + strSex

    strDocId = hashlib.sha1(strDocId.encode()).hexdigest()
            
    data = {"docid": strDocId
                        ,"name": strName
                        ,"surname": strSurname
                        ,"sex": strSex
                        ,"specialization": strSpecialization
                        ,"role": strRole
                        }
    print(data)
    doctor.insert_one(data)

class Name:
    with open("/Users/armi/Documents/nomi_maschili.txt") as file:
        listMale = file.read().splitlines()
    with open("/Users/armi/Documents/nomi_femminili.txt") as file:
        listFemale = file.read().splitlines()
    
    def getMaleName(self):
        max = len(self.listMale)
        randomNum = random.randrange(max)
        return self.listMale[randomNum]

    def getFemaleName(self):
        max = len(self.listFemale)
        randomNum = random.randrange(max)
        return self.listFemale[randomNum]
    

class Surname:
    with open("/Users/armi/Documents/cognomi.txt") as file:
        listSurname = file.read().splitlines()
    
    def getSurname(self):
        max = len(self.listSurname)
        randomNum = random.randrange(max)
        return self.listSurname[randomNum]


class City:
    with open("/Users/armi/Documents/comuni.txt") as file:
        listCity = file.read().splitlines()
    
    def getCity(self):
        max = len(self.listCity)
        randomNum = random.randrange(max)
        return self.listCity[randomNum]


class Specialization:

    listSpecialization = list()
    listSpecialization.append("Cardiochirurgo")
    listSpecialization.append("Anatomo Patologo")
    listSpecialization.append("Reumatologo")
    listSpecialization.append("Ematologo")
    
    def getSpecialization(self):
        max = len(self.listSpecialization)
        randomNum = random.randrange(max)
        return self.listSpecialization[randomNum]


class Role:
    
    listRole = list()
    listRole.append("Head of unit")
    listRole.append("Senior consultant")
    listRole.append("Junior consultant")
    listRole.append("Fellow")
    
    def getRole(self):
        max = len(self.listRole)
        randomNum = random.randrange(max)
        return self.listRole[randomNum]


class Dob:
    
    date_start = "01 Jan, 1950"
    date_end = "31 Dec, 2000"
    dStart = datetime.datetime.strptime(date_start, "%d %b, %Y")
    dEnd = datetime.datetime.strptime(date_end, "%d %b, %Y")

    def getDob(self):
        date = self.dStart + datetime.timedelta(
            seconds=random.randint(0, int((self.dEnd - self.dStart).total_seconds())))
        return datetime.datetime.strftime(date, "%d-%m-%Y")


class Icd9:

    listIcd9 = dict()

    listIcd9["279"] = "Auto immuni"
    listIcd9["039"] = "Infezioni cutanee"
    listIcd9["584"] = "Insufficienza renale"
    listIcd9["410"] = "Cardiopatie"
    listIcd9["784"] = "Epistassi"
    listIcd9["250"] = "Diabete"
    listIcd9["714"] = "Malattie reumatiche"
    listIcd9["280"] = "Anemia"
    listIcd9["535"] = "Intolleranza gastrica"
		
    def getIcd9(self):
        max = len(self.listIcd9)
        randomNum = random.randrange(max)
        return self.listIcd9[randomNum]

        
if __name__ == "__main__":
    
    # for i in range(1000):
    #     populate_patients()

    # populate_doctor()