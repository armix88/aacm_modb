import json
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['patient']
collection_currency = db['icd10']

with open('icd10.json') as f:
    file_data = json.load(f)

chapthers_len = len(file_data['chapters'])

for i in range(chapthers_len):

    blocks_len = len(file_data['chapters'][i]['blocks'])
    for j in range(blocks_len):

        diseases_len = len(file_data['chapters'][i]['blocks'][j])
        for k in range(diseases_len):
            try:
                data = file_data['chapters'][i]['blocks'][j]['diseases'][k]
                collection_currency.insert_one(data)
            except:
                print('Unable to find block ', i, j, k)
 
client.close()
