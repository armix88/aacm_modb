import json
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['patient']
collection = db['atc']

with open('atc.json') as f:
    file_data = json.load(f)

concept_len = len(file_data['conceptList']['concept'])

# print(file_data['conceptList']['concept'])
for i in range(concept_len):
    try:
        data = file_data['conceptList']['concept'][i]
        collection.insert_one(data)
    except:
        print('Unable to find block ', i)
 
client.close()
