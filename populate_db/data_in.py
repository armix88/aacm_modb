import json
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['patient']
collection = db['atc']

with open('in.txt') as f:
    file = f.readlines()
    # file_data = json.load(f)

# concept_len = len(file_data['conceptList']['concept'])

# print(file_data['conceptList']['concept'])
    
    for line in file:
        first, remainder = line.strip().split(' ', maxsplit=1)
        try:
            data = {}
            data['code'] = first
            data['displayName'] = remainder
            collection.insert_one(data)
        except Exception as e:
            print( e)
            # print('Unable to find block ')

        # print(data)
 
client.close()
