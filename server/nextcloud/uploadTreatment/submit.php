<?php

	$username = $_POST["username"];
	$treatid = $_POST["treatid"];
	$info = pathinfo($_FILES['file']['name']);

	$path = '../data/'.$username.'/files/'.'treatments/';
	$newname = $info['basename'];

	if (!file_exists($path.$treatid)) {
		mkdir($path.$treatid);
	}
	$target = $path.$treatid.'/'.$newname;

	move_uploaded_file( $_FILES['file']['tmp_name'], $target);

	shell_exec("cd /var/www/html;php occ files:scan --path=\"$username/files\"");

	$url = "https://fcrlab.unime.it/nextcloud/index.php/apps/files/?dir=/treatments/".$treatid;

    $arr = array ('url'=>$url);

	echo json_encode($arr);
?>
