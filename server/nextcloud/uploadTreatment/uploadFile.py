import json
from flask import Flask,render_template,jsonify,json,request
from flask_cors import CORS, cross_origin
import sys
import os
from werkzeug.utils import secure_filename
import hashlib
import time
from datetime import datetime

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/getDiagnosis": {"origins": "*"}
, r"/uploadFile": {"origins": "*"}})

@app.route("/uploadFile",methods=['POST'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def uploadFile():

    try:
        print(request.form)
        json_data = request.form

        username = json_data['username']
        treatid = json_data['treatid']

        if 'file' in request.files:
            file = request.files['file']

        path = '/data/data/' + username + '/files/treatments/'
        # '../data/' + username + '/files/treatments/'
        print(path)

        filename = secure_filename(file.filename)
        print(filename)
        # basedir = os.path.abspath(os.path.dirname(__file__))

        if not os.path.isdir(path + treatid):
            print("Creo la cartella", path + treatid)
            os.mkdir(path + treatid)
            print("Cartella creata")

        # target = path + treatid.'/' + filename;

        file.save(os.path.join(path + treatid, filename))
        print("File salvato")

        # move_uploaded_file( $_FILES['file']['tmp_name'], $target);

        os.system("docker exec -u www-data nextcloud php occ files:scan --path=\"" + username + "/files\"")

        url = "https://armandoruggeri.it/nextcloud/apps/files/?dir=/Treatments/" + treatid

        return jsonify(status='OK',url=url)

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


if __name__=='__main__':
    app.run(host="0.0.0.0", port=8015)

