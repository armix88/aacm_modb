#!/usr/bin/env python3

import ast
import sys
tLine = []
for line in sys.stdin:
    tLine.append(ast.literal_eval(line.strip()))

for result in tLine:
    print("{0!r}\t{1!r}\t{2!r}\t{3!r}".format(result['_id']['patid'], result['_id']['atc'], result['icd9'], result['effectiveness']))
