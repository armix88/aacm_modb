import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class Connection {
	
	MongoClient mongoClient;

	public DBCollection connect(String strCollection){
		mongoClient = new MongoClient();
		
		DB database = mongoClient.getDB("patient");
		DBCollection collection = database.getCollection(strCollection);
       
		return collection;
	}


//	public MongoCollection<Document> connect(String strCollection){
//		mongoClient = new MongoClient();
//		
//	    MongoDatabase database = mongoClient.getDatabase("patient");
//	    
//	    MongoCollection<Document> collection = database.getCollection(strCollection);
//
//		return collection;
//
//	}
}
