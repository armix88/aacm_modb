import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class Hospitalization {

//	static List<String> listIcd9;
	static Random rand;
	static int maxDate = 2017365;

//	static List<String> listAtc;
	static ArrayList<List<String>> listAtc;
	static List<BasicDBObject> deseases;
	static BasicDBObject searchQuery;
	public static void main(String[] args) throws ParseException {

		long now = System.currentTimeMillis();

		//		listAtc = new ArrayList<String>();
		//		listAtc.add("250");
		//		listAtc.add("401");
		//		listAtc.add("410");
		//		listAtc.add("042");

		listAtc = new ArrayList<List<String>>();

		listAtc.add(Arrays.asList("279", "Auto immuni"));
		listAtc.add(Arrays.asList("584", "Insufficienza renale"));

		listAtc.add(Arrays.asList("039", "Infezioni cutanee"));
		listAtc.add(Arrays.asList("584", "Insufficienza renale"));

		listAtc.add(Arrays.asList("410", "Cardiopatie"));
		listAtc.add(Arrays.asList("401", "Cardiopatie"));
		listAtc.add(Arrays.asList("784", "Epistassi"));

		listAtc.add(Arrays.asList("250", "Diabete"));
		listAtc.add(Arrays.asList( "714", "Malattie reumatiche"));

		listAtc.add(Arrays.asList("280", "Anemia"));
		listAtc.add(Arrays.asList("535", "Intolleranza gastrica"));

		listAtc.add(Arrays.asList("250", "Diabete"));

		rand = new Random();
		//		listIcd9 = new ArrayList<>();

		RandomDate rDate = new RandomDate();

		BasicDBObject query = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		//		BasicDBObject where = new BasicDBObject();

		BasicDBObject value;
		BasicDBObject document;

		List<DBObject> list = new ArrayList<DBObject>();

		List<String> listPatid = new ArrayList<>();
		List<String> listDob = new ArrayList<>();

		fields.put("patid", 1);
		fields.put("dob", 1);

		Connection conn = new Connection();
		DBCollection collection = conn.connect("patient");

		DBCursor cursor = collection.find(query, fields);

		while (cursor.hasNext()) {
			value = (BasicDBObject) cursor.next();
			listPatid.add((String) value.get("patid"));
			listDob.add(rDate.getDateStr((Date) value.get("dob")));
		}

		collection = conn.connect("patient");
		for(int i = 0; i < listPatid.size(); i++) {

			int randomVisits = rand.nextInt(3);

			document = new BasicDBObject();

			deseases = new ArrayList<>();

			for(int j = 0; j <= randomVisits; j++) { //Insert multiple (random) records for the same patient

				int randomNum = getIcd9();
				deseases.add(new BasicDBObject("icd9", listAtc.get(randomNum).get(0)));
				deseases.add(new BasicDBObject("icd9_name", listAtc.get(randomNum).get(1)));
				deseases.add(new BasicDBObject("date", rDate.getDate(listDob.get(i), maxDate)));
			}
			document.append("$set", new BasicDBObject().append("deseases", deseases));

			searchQuery = new BasicDBObject().append("patid", listPatid.get(i));

			collection.update(searchQuery, document);
		}
		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");

		//		1000000 records saved in 30011 ms!
	}
	
	public static int getIcd9() {

		int randomNum = rand.nextInt(listAtc.size() -1 + 1);
		return randomNum;
	}
}
