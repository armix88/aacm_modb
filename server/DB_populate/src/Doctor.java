import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class Doctor {
	
	static int maxRecords = 2000;
	
	public static void main(String[] args) {
		
		String strDocId, strName, strSurname, strSex, strCity;
		Date dDob;
		
		Connection conn = new Connection();
		DBCollection collection = conn.connect("doctor");
		List<DBObject> list = new ArrayList<DBObject>();
		
		RandomDate dob = new RandomDate();
		Name name = new Name();
		Surname surname = new Surname();
		City city = new City();
		
		for(int i = 0; i < maxRecords; i++) {
			
			BasicDBObject document = new BasicDBObject();
			
			if(Math.random() < 0.5) {
				strSex = "M";
				strName = name.getMaleName();
			}
			else {
				strName = name.getFemaleName();
				strSex = "F";
			}
			strSurname = surname.getSurname();
			dDob = dob.getDate(1949365, 1989365);
			strCity = city.getCity();
			
			strDocId = strName + strSurname + strSex + dDob + strCity;
			
			document.put("docid", DigestUtils.sha1Hex(strDocId));
			document.put("name", strName);
			document.put("surname", strSurname);
			document.put("sex", strSex);
			document.put("dob", dDob);
			document.put("city", strCity);
			
			list.add(document);
		}
		
		System.out.println("Now saving " + list.size() + " records...");

		long now = System.currentTimeMillis();
		collection.insert(list);
		
		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");

//		2000 records saved in 156 ms!
	}

}
