import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class Name {

	List<String> listMale;
	List<String> listFemale;
	Random rand;
	
	public Name() {

		rand = new Random();
//		nomi_femminili - 927
//		nomi_maschili - 745
		try {
			listMale = Files.readAllLines(new File("/Users/armi/Documents/nomi_maschili.txt").toPath(), Charset.defaultCharset() );
			listFemale = Files.readAllLines(new File("/Users/armi/Documents/nomi_femminili.txt").toPath(), Charset.defaultCharset() );
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println(Arrays.toString(list.toArray()));
		//	System.out.println(list.get(4));
//			System.out.println(listMale.size());
	}
	
	public String getMaleName() {
		
		int max = listMale.size();
	    int randomNum = rand.nextInt(max);
	    return listMale.get(randomNum).toString();
	}
	
	public String getFemaleName() {
		
		int max = listFemale.size();
	    int randomNum = rand.nextInt(max);
	    return listFemale.get(randomNum).toString();
	}
}
