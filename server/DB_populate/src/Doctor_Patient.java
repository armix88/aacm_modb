import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class Doctor_Patient {

	static List<String> listDoc;
	static Random rand;
	//	int maxDate = 2017365;

	public static void main(String[] args) {

		long now = System.currentTimeMillis();

		rand = new Random();
		listDoc = new ArrayList<>();

		BasicDBObject query = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		BasicDBObject where = new BasicDBObject();

		BasicDBObject valuePat;
		BasicDBObject valueDoc;
		BasicDBObject document;

		List<DBObject> list = new ArrayList<DBObject>();

		fields.put("patid", 1);
		fields.put("city", 1);

		Connection conn = new Connection();
		DBCollection collectionPat = conn.connect("patient");
		DBCollection collectionDoc = conn.connect("doctor");
		DBCollection collectionDocPat = conn.connect("doctor_patient");

		DBCursor cursor = collectionPat.find(query, fields);

		while (cursor.hasNext()) {
			valuePat = (BasicDBObject) cursor.next();

			where.put("city", (String) valuePat.get("city"));

			DBCursor cursorDoc = collectionDoc.find(where);

			while (cursorDoc.hasNext()) {
				valueDoc = (BasicDBObject) cursorDoc.next();
				listDoc.add((String) valueDoc.get("docid"));
			}

			if(listDoc.size() > 0) {
				document = new BasicDBObject();

				document.put("docid", getDoc());
				document.put("patid", valuePat.get("patid"));

				list.add(document);
			}
		}
		System.out.println("Now saving " + list.size() + " records...");

		collectionDocPat = conn.connect("doctor_patient");
		collectionDocPat.insert(list);

		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");
		//		999998 records saved in 1287499 ms!
	}

	public static String getDoc() {

		int max = listDoc.size();
		int randomNum = rand.nextInt(max);
		return listDoc.get(randomNum).toString();
	}

}
