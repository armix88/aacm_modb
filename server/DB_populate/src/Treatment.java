import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class Treatment {

	static List<String> listDoc;
	static Random rand;

	static HashMap<String, List<String>> atc;
	//	int maxDate = 2017365;

	public static void main(String[] args) {

		long now = System.currentTimeMillis();

		atc = new HashMap<String, List<String>>();
		
		atc.put("250", Arrays.asList("A10", "A018", "A32", "C46", "C02C"));
		atc.put("401", Arrays.asList("C02C", "C11", "C01DA", "B14", "A10"));
		atc.put("410", Arrays.asList("C01DA", "A10", "C1D1", "J05A", "B13"));
		atc.put("042", Arrays.asList("J05A", "A018", "C01DA", "J27A", "C02C"));
		
		
		rand = new Random();
		listDoc = new ArrayList<>();

		//		RandomDate rDate = new RandomDate();
		
		String strIcd9 = null;
		String patid = null;

		BasicDBObject query = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
//		BasicDBObject where = new BasicDBObject();

		BasicDBObject valuePat;
		BasicDBObject valueHosp;
		BasicDBObject document;

		List<DBObject> list = new ArrayList<DBObject>();
		HashMap<String, String> patient = new HashMap<>();

		//		List<String> listPatid = new ArrayList<>();
		//		List<String> listDob = new ArrayList<>();

		fields.put("patid", 1);
		fields.put("icd9", 1);

		Connection conn = new Connection();
		DBCollection collectionDocPat = conn.connect("doctor_patient");
		DBCollection collectionHosp = conn.connect("hospitalization");
		DBCollection collectionTreat;

		DBCursor cursorHosp = collectionHosp.find(query, fields);

		while (cursorHosp.hasNext()) {
			valueHosp = (BasicDBObject) cursorHosp.next();
			patient.put((String) valueHosp.get("patid"), (String) valueHosp.get("icd9"));
		}
		
		fields.clear();
		fields.put("patid", 1);
		
		DBCursor cursor = collectionDocPat.find(query, fields);

		while (cursor.hasNext()) {
			valuePat = (BasicDBObject) cursor.next();
			patid = (String) valuePat.get("patid");
			strIcd9 = patient.get(patid);
			
			//			listDob.add((String) value.get("city"));
			//		}

//			where.put("patid", (String) valuePat.get("patid"));


			if(atc.containsKey(strIcd9)) {
				document = new BasicDBObject();

				document.put("patid", patid);
				document.put("icd9", strIcd9);
				document.put("atc", getAtc(strIcd9));


				list.add(document);
			}

			//		for(int i = 0; i < listPatid.size(); i++) {
			////		for(int i = 0; i < 1; i++) {
			//			
			//			document = new BasicDBObject();
			//			
			//			document.put("docid", getDoc());
			//			document.put("patid", listPatid.get(i));
			//			
			//			list.add(document);
			//		}
		}
		System.out.println("Now saving " + list.size() + " records...");
		
		collectionTreat = conn.connect("treatment");
		collectionTreat.insert(list);

		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");

		//		499525 records saved in 18175 ms!


		//		System.out.println(Arrays.toString(list.toArray()));
	}

	public static String getAtc(String strIcd9) {

		List<String> listAtc = atc.get(strIcd9);
		
		if(Math.random() < 0.8) {
			return listAtc.get(0); //quello corretto
		}
		else {
			int randomNum = rand.nextInt(listAtc.size() -1 + 1);
			return listAtc.get(randomNum);
		}
	}

}
