import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class City {

	List<String> listCity;
	Random rand;
	
	public City() {

		rand = new Random();
		//		comuni - 8101
		try {
			listCity = Files.readAllLines(new File("/Users/armi/Documents/comuni.txt").toPath(), StandardCharsets.ISO_8859_1 );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getCity() {
		
		int max = listCity.size();
	    int randomNum = rand.nextInt(max);
	    return listCity.get(randomNum).toString();
	}
	
}
