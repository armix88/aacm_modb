import org.bson.Document;

import com.mongodb.Block;
    

public class MainTest {
	
	static int maxRecords = 2000;
	

	public static void main(String[] args) {
		
//		Icd9 icd9 = new Icd9();
		

//		int maxDate = 2017365;
//		Date d;
//		try {
//			d = new SimpleDateFormat("dd.MM.yyyy").parse("9.01.1995");
//
//			String g = new SimpleDateFormat("yyyyD").format(d);
//			int julian = Integer.parseInt(g);
//			System.out.println(julian);
//		
//		String checkDate = String.valueOf(julian);
//		
//		if(checkDate.length() < 6) {
//			System.out.println("Too Short!");
//			julian = Integer.parseInt(checkDate.substring(0, 4) + "00" + checkDate.substring(4));
//			System.out.println(julian);
//			
//		}
//		else if(checkDate.length() < 7) {
//			System.out.println("Too Short!");
//			julian = Integer.parseInt(checkDate.substring(0, 4) + "0" + checkDate.substring(4));
//			System.out.println(julian);
//			
//		}
//		Random rand = new Random();
//		
//		int nMin = 1995173;
//		int randomNum = rand.nextInt((maxDate - nMin)) + nMin;
//		
//		Date date = new SimpleDateFormat("yyyyDD").parse(Integer.toString(randomNum));
//		g = new SimpleDateFormat("dd.MM.yyyy").format(date);
//		System.out.println(g);
//		
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		
		
		Block<Document> printBlock = new Block<Document>() {
	        @Override
	        public void apply(final Document document) {
	            System.out.println(document.toJson());
	        }
	    };
	    
//	    MongoClient mongoClient = MongoClients.create();
//	    MongoDatabase database = mongoClient.getDatabase("patient");
//	    MongoCollection<Document> collection = database.getCollection("hospitalization");
	    
//	    collection.aggregate(
//	    		  Arrays.asList(
//	    		          Aggregates.match(Filters.eq("patid", "f82dcde22b514f5b0174b4d742eb6e9a39c44d6b")),
//	    		          Aggregates.group("$effectiveness", Accumulators.sum("count", 1))
//	    		  )
//	    		).forEach(printBlock);

	    
//	    collection.aggregate(
//	    	      Arrays.asList(
//	    	          Aggregates.project(
//	    	              Projections.fields(
//	    	                    Projections.excludeId(),
//	    	                    Projections.include("name"),
//	    	                    Projections.computed(
//	    	                            "firstCategory",
//	    	                            new Document("$arrayElemAt", Arrays.asList("$categories", 0))
//	    	                    )
//	    	              )
//	    	          )
//	    	      )
//	    	).forEach(printBlock);
	    
}

}
