import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class Patient {
	
	static int maxRecords = 1000000;

	static Random rand;
	static int maxDate = 2017365;
	static ArrayList<List<String>> listAtc;
	static List<BasicDBObject> deseasesList;
	static BasicDBObject searchQuery;
	
	public static void main(String[] args) throws ParseException {

		long now = System.currentTimeMillis();
		
		String strPatId, strName, strSurname, strSex, strCity, strDob;
		Date dDob;
		
		Connection conn = new Connection();
		DBCollection collection = conn.connect("patient");
		List<DBObject> list = new ArrayList<DBObject>();
		BasicDBObject documentDeseases;
		
		RandomDate dob = new RandomDate();
		Name name = new Name();
		Surname surname = new Surname();
		City city = new City();

		listAtc = new ArrayList<List<String>>();

		listAtc.add(Arrays.asList("279", "Auto immuni"));
		
		listAtc.add(Arrays.asList("039", "Infezioni cutanee"));
		listAtc.add(Arrays.asList("584", "Insufficienza renale"));
		
		listAtc.add(Arrays.asList("410", "Cardiopatie"));
		listAtc.add(Arrays.asList("784", "Epistassi"));

		listAtc.add(Arrays.asList("250", "Diabete"));
		listAtc.add(Arrays.asList( "714", "Malattie reumatiche"));

		listAtc.add(Arrays.asList("280", "Anemia"));
		listAtc.add(Arrays.asList("535", "Intolleranza gastrica"));
		
		listAtc.add(Arrays.asList("250", "Diabete"));
		
		rand = new Random();

		RandomDate rDate = new RandomDate();
		
		for(int i = 0; i < maxRecords; i++) {

			int randomVisits = rand.nextInt(3);
			BasicDBObject document = new BasicDBObject();
			
			if(Math.random() < 0.5) {
				strSex = "M";
				strName = name.getMaleName();
			}
			else {
				strName = name.getFemaleName();
				strSex = "F";
			}
			strSurname = surname.getSurname();
			dDob = dob.getDate(1949365, 1999365);
			strDob = rDate.getDateStr(dDob);
			strCity = city.getCity();
			strPatId = strName + strSurname + strSex + dDob + strCity;
			
			document.put("patid", DigestUtils.sha1Hex(strPatId));
			document.put("name", strName);
			document.put("surname", strSurname);
			document.put("sex", strSex);
			document.put("dob", dDob);
			document.put("city", strCity);
			
			deseasesList = new ArrayList<>();
			for(int j = 0; j <= randomVisits; j++) { //Insert multiple (random) records for the same patient

				int randomNum = getIcd9();

				documentDeseases = new BasicDBObject();
				
				documentDeseases.put("icd9", listAtc.get(randomNum).get(0));
				documentDeseases.put("icd9_name", listAtc.get(randomNum).get(1));
				documentDeseases.put("date", rDate.getDate(strDob, maxDate));

				deseasesList.add(documentDeseases);
			}
			document.put("deseases", deseasesList);
			list.add(document);
		}
		System.out.println("Now saving " + list.size() + " records...");

		collection.insert(list);
		
		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");
		
//		1000000 records saved in 8965 ms!

	}
	
	public static int getIcd9() {
		
		int randomNum = rand.nextInt(listAtc.size() -1 + 1);
		return randomNum;
	}
}
