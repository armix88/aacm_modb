import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class RandomDate {

//	public String getDate(int min, int max) {
//
//		String dob = null;
//		try { //1949365, 1999365
//			//	dob = getDate(1999365);
//			dob = calculateJulianDate(min, max);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return dob;
//	}

//	public String getDate(String min, int max) {
//
//		String dob = null;
//		try {
//			dob = calculateJulianDate(min, max);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return dob;
//	}

	public Date getDate(int min, int max) {

		Date dob = null;
		try { //1949365, 1999365
			//	dob = getDate(1999365);
			dob = calculateJulian(min, max);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dob;
	}
	
	public Date getDate(String min, int max) {

		Date dob = null;
		try {
			dob = calculateJulian(min, max);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dob;
	}

//	private String calculateJulianDate(int min, int max) throws ParseException {
//
//		Random rand = new Random();
//		int randomNum = rand.nextInt((max - min)) + min;
//		return getDateStr(randomNum);
//	}
//
//	private String calculateJulianDate(String min, int max) throws ParseException {
//
//		Random rand = new Random();
//		int nMin = getDateInt(min);
//		int randomNum = rand.nextInt((max - nMin)) + nMin;
//		return getDateStr(randomNum);
//	}
	
	private Date calculateJulian(int min, int max) throws ParseException {

		Random rand = new Random();
		int randomNum = rand.nextInt((max - min)) + min;
		return getDate(randomNum);
	}
	
	private Date calculateJulian(String min, int max) throws ParseException {

		int nMin = getDateInt(min);
		return calculateJulian(nMin, max);
	}

	private Date getDate(int julian) throws ParseException{

		Date date = new SimpleDateFormat("yyyyD").parse(Integer.toString(julian));
		return date; 
	}
	
	private String getDateStr(int julian) throws ParseException{

		Date date = new SimpleDateFormat("yyyyD").parse(Integer.toString(julian));
		return getDateStr(date); 
	}

	public String getDateStr(Date date) throws ParseException{

		String g = new SimpleDateFormat("dd.MM.yyyy").format(date);
		return g; 
	}

	private int getDateInt(String strDate) throws ParseException{

		Date d = new SimpleDateFormat("dd.MM.yyyy").parse(strDate);
		String g = new SimpleDateFormat("yyyyD").format(d);
		int julian = Integer.parseInt(g);
		
		String checkDate = String.valueOf(julian);

		if(checkDate.length() < 6) {
			julian = Integer.parseInt(checkDate.substring(0, 4) + "00" + checkDate.substring(4));

		}
		else if(checkDate.length() < 7) {
			julian = Integer.parseInt(checkDate.substring(0, 4) + "0" + checkDate.substring(4));

		}
		return julian; 
	}
}
