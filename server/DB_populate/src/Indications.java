import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class Indications {

	static List<String> listDoc;
	static Random rand;

	static HashMap<String, List<String>> atc;

	public static void main(String[] args) {

		long now = System.currentTimeMillis();

		ArrayList<List<String>> entries = new ArrayList<List<String>>();

		entries.add(Arrays.asList("L04AD01", "Ciclosporidina", "279", "Auto immuni", "8"));
		entries.add(Arrays.asList("L04AD01", "Ciclosporidina", "584", "Insufficienza renale", "-3"));
		
		entries.add(Arrays.asList("J01GB03", "Aminoglicosidi", "039", "Infezioni cutanee", "4"));
		entries.add(Arrays.asList("J01GB03", "Aminoglicosidi", "584", "Insufficienza renale", "-5"));
		
		entries.add(Arrays.asList("B01AC06", "Cardioaspirina", "410", "Cardiopatie", "8"));
		entries.add(Arrays.asList("B01AC06", "Cardioaspirina", "401", "Cardiopatie", "8"));
		entries.add(Arrays.asList("B01AC06", "Cardioaspirina", "784", "Epistassi", "-6"));

		entries.add(Arrays.asList("H02AB10", "Cortisone", "250", "Diabete", "-3"));
		entries.add(Arrays.asList("H02AB10", "Cortisone", "714", "Malattie reumatiche", "6"));

		entries.add(Arrays.asList("B03AE01", "Ferro", "280", "Anemia", "7"));
		entries.add(Arrays.asList("B03AE01", "Ferro", "535", "Intolleranza gastrica", "-3"));
		
		entries.add(Arrays.asList("A10", "Insulina", "250", "Diabete", "8"));

		BasicDBObject document;

		List<DBObject> list = new ArrayList<DBObject>();

		Connection conn = new Connection();
		DBCollection collectionIndications;

		for (List<String> entry : entries) {

			document = new BasicDBObject();

			document.put("atc", entry.get(0));
			document.put("atc_name", entry.get(1));
			document.put("icd9", entry.get(2));
			document.put("icd9_name", entry.get(3));
			document.put("effectiveness", Integer.valueOf(entry.get(4)));

			list.add(document);
		}

		System.out.println("Now saving " + list.size() + " records...");
		
		collectionIndications = conn.connect("indications");
		collectionIndications.insert(list);

		System.out.println(list.size() + " records saved in " + (System.currentTimeMillis() - now) + " ms!");

		//		499525 records saved in 18175 ms!
	}
}
