import sys
import matplotlib.pyplot as plt
import csv
import pandas as pd

# file_name = sys.argv[1]
# file_name = "results/proc2/activity_1-07.txt"
# file_name2 = "results/proc2/activity_mongo_1-07.txt"
# file_name = "results/map2/activity_1-07.txt"
file_name = "results/proc/activity_python-13.07.txt"
# file_name2 = "results/map2/activity_mongo_1-07.txt"
file_name2 = "results/proc/activity_mongo-13-07.txt"

df = pd.read_csv(file_name, sep=',')
df2 = pd.read_csv(file_name2, sep=',')

# print(df['Time'].min())
# print(df['Time'].max())

# print(df2['Time'].min())
# print(df2['Time'].max())

# y = df['CPU']
# x = df['Time']
# plt.plot(x,y)
# print(a)
# a.plot()
# ts = pd.Series(pd.read_csv(file_name, sep=','))
# ts.plot()

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)

ax.plot(df['Time'], df['CPU']/100, '-', lw=1, color='r', label='CPU Python')

ax.set_ylabel('CPU (%)', color='r')
ax.set_xlabel('Time (s)')
ax.set_ylim(0., max(max(df['CPU']), max(df2['CPU']))/100 * 1.2)
ax.set_ylabel('CPU', color='r')
# ax2 = ax.twinx()

# ax2.plot(df['Time'], df['Virtual(MB)'], '-', lw=1, color='b')
# ax2.set_ylim(0., max(df['Virtual(MB)']) * 1.2)
# ax2.set_ylabel('Virtual Memory (MB)', color='b')


ax.plot(df2['Time'], df2['CPU']/100, '-', lw=1, color='b', label='CPU MongoDB')
# ax.set_ylim(0., max(max(df['CPU']), max(df2['CPU'])) * 1.2)
ax.set_xlim(min(df['Time']), max(df['Time']))

ax.legend()
ax.grid()   

# fig.savefig(plot)

# plt.show()

# fig2 = plt.figure()
ax = fig.add_subplot(2, 1, 2)

ax.plot(df['Time'], df['Real(MB)'], '-', lw=1, color='r', label='Memory Python')
ax.set_ylabel('Real Memory (MB)', color='r')
ax.set_xlabel('Time (s)')
ax.set_ylim(0., max(max(df['Real(MB)']), max(df2['Real(MB)'])) * 1.2)

# ax2 = ax.twinx()

ax.plot(df2['Time'], df2['Real(MB)'], '-', lw=1, color='b', label='Memory MongoDB')
# ax.set_ylim(0., max(df2['Virtual(MB)']) * 1.2)
ax.set_xlim(min(df['Time']), max(df['Time']))

ax.legend()
ax.grid()

# fig.savefig(plot)

# plt.show()

# fig3 = plt.figure()
# ax = fig.add_subplot(2, 2, 3)

# ax.plot(df['Time'], df['Virtual(MB)'], '-', lw=1, color='r', label='Virtual Memory Python')
# ax.set_ylabel('Virtual(MB)', color='r')
# ax.set_xlabel('Time (s)')
# ax.set_ylim(0., max(df['Virtual(MB)']) * 1.2)

# # ax2 = ax.twinx()

# ax.plot(df2['Time'], df2['Virtual(MB)'], '-', lw=1, color='b', label='Virtual Memory MongoDB')
# ax.set_ylim(0., max(df2['Virtual(MB)']) * 1.2)
# ax.set_xlim(min(df['Time']), max(df['Time']))

# ax.legend()
# ax.grid()


plt.show()
