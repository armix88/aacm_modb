from datetime import datetime
import pymongo
from bson.json_util import dumps
import json
import time
import random
import logging
import ast
import multiprocessing

from multiprocessing_mapreduce import SimpleMapReduce

client = pymongo.MongoClient()
db = client["patient"]
collection = db["patient"]

def getResult():
     # query = {"$or" : [{"patid": "0149239e44e656d668808959c24ed2dfce1530bc"}, {"patid": "2a526fec055da123beaac90fcf94df36ed232105"}], "diseases.date":  {"$gte": datetime(2012, 1, 1)} }
     # query = {"$or" : [{"patid": "13063f6aa5a7f8aba704d61e9803584f604067f3"}, {"patid": "92028bce50af6280504a3db5d54c83943d442ef7"}], "diseases.date":  {"$gte": datetime(2012, 1, 1)} }
     # query = { "patid": "7808d14d6a0cc38c89d6c24a642e5c5f21757f71" }
     # query = { "patid": "002961a5cac7737384af5d729b736eddf43edd10" }

     query = { "diseases.date":  {"$gte": datetime(2012, 1, 1)} }

     pipeline = [
               {'$match': query }
               ,{'$unwind': '$diseases'}   
               , {'$group':
               {
                    '_id': {
                         'patid' : '$patid', 
                         'icd9' : '$diseases.icd9'
                         } 
                    }}

               , {'$lookup': 
                    {'from' : 'indications',
                    'localField' : '_id.icd9',
                    'foreignField' : 'icd9',
                    'as' : 'test'}}        
               , {'$unwind': '$test'}

               # , {'$limit': 20 }
               , {'$group':
               {
                    '_id': {
                         'patid' : '$_id.patid', 
                         'atc' : '$test.atc',
                         'icd9' : '$test.icd9' 
                         }
                    ,
                    'effectiveness': {  '$sum' : '$test.effectiveness' } 
                    }}

                    , {'$group':
                    {
                    '_id': {
                         'patid' : '$_id.patid', 
                         'atc' : '$_id.atc'
                         }
                    , 'icd9': {'$addToSet': '$_id.icd9' }
                    , 'effectiveness':  { "$sum": "$effectiveness" }
                    , 'icd9Count': { "$sum": 1 }
               }
               }    
               , {'$sort': {'patid' : 1 }}
               ]

     result = collection.aggregate(pipeline, allowDiskUse=True)

     return result
     
     
def procedural(result):
     temp1 = dict()
     temp2 = dict()
     final = dict()
     for element in result:
          patid = element['_id']['patid']
          atc = element['_id']['atc']
          icd9 = element['icd9']
          effectiveness = element['effectiveness']

          key = patid + "***" + str(effectiveness)
          if patid in temp2:
               if effectiveness > temp2[patid]:
                    temp2[patid] = effectiveness
          else:
               temp2[patid] = effectiveness

          if key not in temp1:
               temp1[key] = (atc, icd9)
     
     for patid in temp2:
          key = patid + "***" + str(temp2[patid])
          final[key] = temp1[key]

     return final

def get_patients(result):
     """Read a file and return a sequence of
     (word, occurences) values.
     """
     output = []
     temp = []
     
     temp.append(result['_id']['patid'])
     temp.append(result['_id']['atc'])
     temp.append(result['icd9'])
     temp.append(result['effectiveness'])
     output.append(tuple(temp))
     
     return output

def count_words(item):
     """Convert the partitioned data for a word to a
     tuple containing the word and the number of occurences.
     """
     patid, tpl = item
     return (patid, max(eff for _, eff in tpl))


def mapReduce(result):

     import operator

     mapper = SimpleMapReduce(get_patients, count_words)
     therapy_count = mapper(result)
     
     return therapy_count


if __name__ == '__main__':

     logging.basicConfig(filename='app' + str(time.time()) + '.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s',level=logging.INFO)

     # logging.info("getResult() started")
     # current_time = time.time()
     # result = getResult()
     # duration = time.time()-current_time
     # logging.info("getResult() completed in {0!r} ".format(duration))

     # result = json.loads(dumps(result))
     # for res in result:
     #    print (res)

     logging.info("file_read() started")
     current_time = time.time()
     result = []
     f = open("result_completo.txt")
     for line in f.readlines():
          result.append(ast.literal_eval(line.strip()))
     duration = time.time()-current_time
     # logging.debug("\n".join(r))
     logging.info("file_read() completed in {0!r} ".format(duration))

     # logging.info("procedural() started")
     # current_time = time.time()
     # r = procedural(result)
     # duration = time.time()-current_time
     # # logging.debug("\n".join(r))
     # print("\n".join(r))
     # logging.info("procedural() completed in {0!r} for {1!r} results".format(duration, len(r)))

     # logging.info("mapReduce() started")
     # current_time = time.time()
     # r = mapReduce(result)
     # duration = time.time()-current_time
     # # logging.debug("\n".join(r))
     # logging.info("mapReduce() completed in {0!r} for {1!r} results".format(duration, len(r)))

client.close()
