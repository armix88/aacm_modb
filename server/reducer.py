#!/usr/bin/env python3

import sys

result = {}
for line in sys.stdin:
    line = line.strip()

    r = line.split('\t')
    patid = r[0]
    if patid in result:
        if int(r[3]) > int(result[patid][2]):
            result[patid] = r[1:]
    else:
        result[patid] = r[1:]
print(result)
