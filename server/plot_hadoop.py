import sys
import matplotlib.pyplot as plt
import csv
import pandas as pd

# file_name = sys.argv[1]
# file_name = "results/proc2/activity_1-07.txt"
# file_name2 = "results/proc2/activity_mongo_1-07.txt"
# file_name = "results/map2/activity_1-07.txt"
# file_name2 = "results/map2/activity_mongo_1-07.txt"
# file_name1 = "results/test3_2_slaves_8map_4red/activity-master-9-07.txt"
# file_name2 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave1-9-07.txt"
# file_name3 = "results/test3_2_slaves_8map_4red/activity-NameNode-master-9-07.txt"
# file_name4 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave1-9-07.txt"
# file_name5 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave2-9-07.txt"
# file_name6 = "results/test3_2_slaves_8map_4red/activity-ResourceManager-master-9-07.txt"
# file_name7 = "results/test3_2_slaves_8map_4red/activity-SecondaryNameNode-master-9-07.txt"

# file_name1 = "results/test3_2_slaves_8map_4red/activity-master-9-07.txt"
# file_name2 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave1-9-07.txt"
# file_name3 = "results/test3_2_slaves_8map_4red/activity-NameNode-master-9-07.txt"
# file_name4 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave1-9-07.txt"
# file_name5 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave2-9-07.txt"
# file_name6 = "results/test3_2_slaves_8map_4red/activity-ResourceManager-master-9-07.txt"
# file_name7 = "results/test3_2_slaves_8map_4red/activity-SecondaryNameNode-master-9-07.txt"

file_name1 = "results/test3_2_slaves_8map_4red/activity-master-9-07.txt"
file_name2 = "results/test3_2_slaves_8map_4red/activity-NameNode-master-9-07.txt"
file_name3 = "results/test3_2_slaves_8map_4red/activity-ResourceManager-master-9-07.txt"
file_name4 = "results/test3_2_slaves_8map_4red/activity-SecondaryNameNode-master-9-07.txt"
file_name5 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave1-9-07.txt"
file_name6 = "results/test3_2_slaves_8map_4red/activity-DataNode-slave1-9-07.txt"
file_name7 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave2-9-07.txt"
file_name8 = "results/test3_2_slaves_8map_4red/activity-DataNode-slave2-9-07.txt"


# file_name9 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave3-9-07.txt"
# file_name10 = "results/test3_2_slaves_8map_4red/activity-DataNode-slave3-9-07.txt"
# file_name11 = "results/test3_2_slaves_8map_4red/activity-NodeManager-slave4-9-07.txt"
# file_name12 = "results/test3_2_slaves_8map_4red/activity-DataNode-slave4-9-07.txt"


df1 = pd.read_csv(file_name1, sep=',')
df2 = pd.read_csv(file_name2, sep=',')
df3 = pd.read_csv(file_name3, sep=',')
df4 = pd.read_csv(file_name4, sep=',')
df5 = pd.read_csv(file_name5, sep=',')
df6 = pd.read_csv(file_name6, sep=',')
df7 = pd.read_csv(file_name7, sep=',')
df8 = pd.read_csv(file_name8, sep=',')

# df9 = pd.read_csv(file_name5, sep=',')
# df10 = pd.read_csv(file_name6, sep=',')
# df11 = pd.read_csv(file_name7, sep=',')
# df12 = pd.read_csv(file_name8, sep=',')

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.set_ylabel('CPU (%)', color='r')
ax.set_xlabel('Time (s)')
ax.set_xlim(min(df1['Time']), max(df1['Time']))
ax.set_ylim(0., max(max(df2['CPU']), max(df3['CPU']), max(df4['CPU']), max(df5['CPU']), max(df6['CPU']), max(df7['CPU']), max(df8['CPU']))/100 * 1.8)
ax.set_ylabel('CPU', color='r')
# ax2 = ax.twinx()

# ax2.plot(df['Time'], df['Real(MB)'], '-', lw=1, color='b')
# ax2.set_ylim(0., max(df['Real(MB)']) * 1.2)
# ax2.set_ylabel('Real Memory (MB)', color='b')


ax.plot(df2['Time'], df2['CPU']/100, lw=1, label='CPU NameNode-master')
ax.plot(df3['Time'], df3['CPU']/100, lw=1, label='CPU ResourceManager-master')
ax.plot(df4['Time'], df4['CPU']/100, lw=1, label='CPU SecondaryNameNode-master')
ax.plot(df5['Time'], df5['CPU']/100, lw=1, label='CPU NodeManager-slave1')
ax.plot(df6['Time'], df6['CPU']/100, lw=1, label='CPU DataNode-slave1')
ax.plot(df7['Time'], df7['CPU']/100, lw=1, label='CPU NodeManager-slave2')
ax.plot(df8['Time'], df8['CPU']/100, lw=1, label='CPU DataNode-slave2')

# ax.plot(df9['Time'], df9['CPU']/100, lw=1, label='CPU NodeManager-slave3')
# ax.plot(df10['Time'], df10['CPU']/100, lw=1, label='CPU DataNode-slave3')
# ax.plot(df11['Time'], df11['CPU']/100, lw=1, label='CPU NodeManager-slave4')
# ax.plot(df12['Time'], df12['CPU']/100, lw=1, label='CPU DataNode-slave4')

ax.legend(loc='upper right')
ax.grid()   

plt.show()
