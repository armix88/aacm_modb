import multiprocessing
import string

from multiprocessing_mapreduce import SimpleMapReduce


def file_to_words(filename):
    """Read a file and return a sequence of
    (word, occurences) values.
    """
    STOP_WORDS = set([
        'a', 'an', 'and', 'are', 'as', 'be', 'by', 'for', 'if',
        'in', 'is', 'it', 'of', 'or', 'py', 'rst', 'that', 'the',
        'to', 'with',
    ])
    TR = str.maketrans({
        p: ' '
        for p in string.punctuation
    })

    print('{} reading {}'.format(
        multiprocessing.current_process().name, filename))
    output = []

    with open(filename, 'rt') as f:
        for line in f:
            # Skip comment lines.
            if line.lstrip().startswith('..'):
                continue
            line = line.translate(TR)  # Strip punctuation
            for word in line.split():
                word = word.lower()
                if word.isalpha() and word not in STOP_WORDS:
                    output.append((word, 1))
    return output



def get_patients(result):
    """Read a file and return a sequence of
    (word, occurences) values.
    """
    
    output = []

    patid = []
    atc = []
    temp = []
    effectiveness = []


    for element in result:
        temp = []

        patid.append(element['_id']['patid'])
        atc.append(element['_id']['atc'])
        effectiveness.append(element['effectiveness'])

        temp.append(element['_id']['patid'])
        temp.append(element['_id']['atc'])
        temp.append(element['effectiveness'])
        output.append(tuple(temp))

    return output


def count_words(item):
    """Convert the partitioned data for a word to a
    tuple containing the word and the number of occurences.
    """
    atc, patid, effectiveness = item
    return (atc, patid, max(effectiveness))


if __name__ == '__main__':
    import operator
    import glob

    # input_files = glob.glob('*.rst')

    mapper = SimpleMapReduce(get_patients, count_words)
    word_counts = mapper(results)
    word_counts.sort(key=operator.itemgetter(1))
    word_counts.reverse()

    print('\nTOP 20 WORDS BY FREQUENCY\n')
    top20 = word_counts[:20]
    longest = max(len(word) for word, count in top20)
    for word, count in top20:
        print('{word:<{len}}: {count:5}'.format(
            len=longest + 1,
            word=word,
            count=count)
        )
