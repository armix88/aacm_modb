import json
# from flask import Flask, Response, request, jsonify
# from flask import Flask,render_template,jsonify,json,request
# from flask_cors import CORS, cross_origin
from web3 import Web3
import base64
import sys
import os
import pymongo
import hashlib
import time

from datetime import datetime
import patient
from bson.objectid import ObjectId


# Get stored abi and contract_address
with open("smart_contract/build/contracts/Hospital.json", 'r') as f:
    datastore = json.load(f)
    abi = datastore["abi"]
    # bytecode = datastore['bin']

# threshold = 0
# Initializing flask app
# app = Flask(__name__)
# app.config['CORS_HEADERS'] = 'Content-Type'

# cors = CORS(app, resources={r"/getDiagnosis": {"origins": "*"}
# , r"/addPatient": {"origins": "*"}
# , r"/getPatientList": {"origins": "*"}
# , r"/getDoctorList": {"origins": "*"}
# , r"/getTreatments": {"origins": "*"}
# , r"/getIcdList": {"origins": "*"}
# , r"/getAtcList": {"origins": "*"}
# , r"/meetingResult": {"origins": "*"}
# , r"/addTreatment": {"origins": "*"}})

# import ipfsApi
# ipfs = ipfsApi.Client(host='https://ipfs.infura.io', port=5001)

# Read the configuration from the file config.json
def read_config():
    with open('config.json') as file:
        config = json.load(file)
        return config

config = read_config()

privateKey = base64.b64decode(config["p_key"]).decode()
chainId = config["chainId"]
contract_address = config["contract_address"]
defaultAccount = config["defaultAccount"]
HTTPProvider = config['HTTPProvider']
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))

w3.eth.defaultAccount = defaultAccount

nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount) - 1

client = pymongo.MongoClient(os.environ["DB_HOST_PORT"])
db = client.patient

def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt


def sendTransaction():
    print("Scanning for transactions")
    try:

        txList = patient.getPendingTransactions()
        if(len(txList)) < 1:
            return 0
        print(len(txList), "transaction(s) found")
        for txTemp in txList:

            id = txTemp['_id']['$oid']
            tx = txTemp['tx']
            treatid = txTemp['treatid']
            document = txTemp['document']

            nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

            tx['nonce'] = nonce

            signed = w3.eth.account.signTransaction(tx, privateKey)
            tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

            # Wait for transaction to be mined...
            user_data = waitForReceipt(tx_hash, 240, 4)

            strHash = user_data['transactionHash'].hex()
            # print("user_data", user_data)
            
            # col = document.split(".")
            # for c in reversed(col):

            # record['hash'] = strHash
            # document: strHash}
            db.treatment_1.find_one_and_update({'treatid':treatid}, {"$set": {document: strHash} })
        
            db.pending_transactions.find_one_and_update({'_id': ObjectId(id)}, {"$set": {
                                        "status": "Completed"}})

    except Exception as e:
        print(str(e))
        # print(user_data['transactionHash'].hex())
        # print("Hash returned ", file=sys.stdout)
        # print(user_data['transactionHash'].hex(), file=sys.stdout)
    # return user_data['transactionHash'].hex()
    return 1



# import time, threading
if __name__=='__main__':

    WAIT_SECONDS = 30
    while True:
      ret_val = sendTransaction()
      
      if(ret_val == 0):
        time.sleep(WAIT_SECONDS)  
    # threading.Timer(WAIT_SECONDS, sendTransaction).start()

    # sendTransaction()
    # app.run(host="0.0.0.0", port=8001)
