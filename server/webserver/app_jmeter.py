import json
from flask import Flask,render_template,jsonify,json,request
from flask_cors import CORS, cross_origin
from web3 import Web3
import base64
import sys
import os
from werkzeug.utils import secure_filename
import pymongo
import hashlib
import time
from datetime import datetime
import patient


# Get stored abi and contract_address
with open("smart_contract/build/contracts/Hospital.json", 'r') as f:
    datastore = json.load(f)
    abi = datastore["abi"]
    # bytecode = datastore['bin']

threshold = 0
# Initializing flask app
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/getDiagnosis": {"origins": "*"}
, r"/addPatient": {"origins": "*"}
, r"/getPatientList": {"origins": "*"}
, r"/getDoctorList": {"origins": "*"}
, r"/getTreatments": {"origins": "*"}
, r"/getIcdList": {"origins": "*"}
, r"/getAtcList": {"origins": "*"}
, r"/meetingResult": {"origins": "*"}
, r"/addTreatment": {"origins": "*"}})

import ipfsApi
ipfs = ipfsApi.Client(host='https://ipfs.infura.io', port=5001)

# Read the configuration from the file config.json
def read_config():
    with open('config.json') as file:
        config = json.load(file)
        return config

config = read_config()

privateKey = base64.b64decode(config["p_key"]).decode()
chainId = config["chainId"]
contract_address = config["contract_address"]
defaultAccount = config["defaultAccount"]
HTTPProvider = config['HTTPProvider']
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))

w3.eth.defaultAccount = defaultAccount

nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount) - 1

client = pymongo.MongoClient(os.environ["DB_HOST_PORT"])
db = client.patient

@app.route("/addPatient",methods=['POST'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def addPatient():

    try:
        json_data = request.form

        name = json_data['name']
        surname = json_data['surname']
        sex = json_data['sex']
        dob = json_data['dob']
        city = json_data['city']

        patid = name + surname + sex + dob + city
        patid = hashlib.sha1(patid.encode()).hexdigest()

        db.patient.insert_one({
            'patid':patid,'name':name,'surname':surname,'sex':sex,'dob':dob,'city':city
            })

        return jsonify(status='OK',message='inserted successfully')

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/addTreatment",methods=['POST'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def addTreatment():
    try:
        # print(request.form)
        json_data = request.form
        username = "root"

        patid = json_data['patid']
        docid = json_data['docid']
        date = json_data['date']
        icd = json_data['icd']
        atc = json_data['atc']
        description = json_data['description']
        with_ipfs_upload = int(json_data['with_ipfs_upload'])
        with_nc_upload = int(json_data['with_nc_upload'])
        with_blockchain = int(json_data['with_blockchain'])
        status = 'In progress'

        treatid = patid + docid + date + icd
        treatid = hashlib.sha1(treatid.encode()).hexdigest()

        # check if the post request has the file part
        # if 'file' not in request.files:
            # print("No file found in message", file=sys.stdout)
            # return jsonify({"data": "No file found in message"}), 200

        # file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        # if file.filename == '':
            # print("Unable to read file", file=sys.stdout)
            # return jsonify({"data": "Unable to read file"}), 200

        if(with_ipfs_upload > 0 or with_nc_upload > 0):
            if 'file' in request.files:
                file = request.files['file']

        url = ""
        if(with_ipfs_upload > 0):
            url = fileUploadIpfs(file)
            
        # Uncomment below for IPFS upload (takes some time while testing)
        if(with_nc_upload > 0):
            url = fileUpload(username, treatid, file)

        icdList = []
        icdListTmp = icd.split(',')
        for element in icdListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            icdDict = {}
            icdDict['icd'] = first
            icdDict['icd_name'] = remainder
            icdList.append(icdDict)

        atcList = []
        atcListTmp = atc.split(',')
        for element in atcListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            atcDict = {}
            atcDict['atc'] = first
            atcDict['atc_name'] = remainder
            atcList.append(atcDict)

        timestamp = datetime.now()

        db.treatment_1.insert_one({
            'treatid':treatid,'patid':patid,'docid':docid,'icd':icdList,'atc':atcList,'date':date,'description':description,'documentation':url,'timestamp':timestamp,'status':status
            })

        # strHash = ""
        if(with_blockchain > 0): 
            # strHash = 
            newHospitalization(treatid, patid, docid, icd, atc, date, description, url)

        # db.treatment_1.find_one_and_update({'treatid':treatid}, {"$set": {
        #                                 "hash": strHash}})

        return jsonify(status=status, message="message", data={'treatid':treatid, 'patid':patid, 'docid':docid})

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/runMatch",methods=['POST'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def runMatch():
    try:
        print(request.form)
        json_data = request.form
        with_blockchain = int(json_data['with_blockchain'])

        patid = json_data['patid']
        docid = json_data['docid']
        date = json_data['date']
        icd = json_data['icd']
        atc = json_data['atc']
        status = 'In progress'

        treatid = patid + docid + date + icd
        treatid = hashlib.sha1(treatid.encode()).hexdigest()

        icdList = []
        icdListTmp = icd.split(',')
        for element in icdListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            icdDict = {}
            icdDict['icd'] = first
            icdDict['icd_name'] = remainder
            icdList.append(icdDict)

        atcList = []
        atcListTmp = atc.split(',')
        for element in atcListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            atcDict = {}
            atcDict['atc'] = first
            atcDict['atc_name'] = remainder
            atcList.append(atcDict)

        res_patient, res_deceased = runMatchMaker(patid, icdList, atcList)

        message = "Nothing found for this treatment"
        status = "Success"

        if len(res_deceased) > 0:
            message = "Warning! A similar treatment lead to death in " + str(len(res_deceased)) + " case(s).\nA meeting with the department is recommended!"
            status = "Warning"
        else:
            for patient in res_patient:
                for icdTemp in icdList:
                    for atcTemp in atcList:
                        if patient['_id']['icd'] == icdTemp['icd'] and patient['_id']['atc'] == atcTemp['atc']:
                            if patient['effectiveness'] > threshold:
                                message = "Treatment is ok"
                                status = "Success"
                            else:
                                message = "There is a problem with the treatment suggested for patid".join([patid, " and atc", atc, " for icd", icd, "\nA meeting with the department is recommended!"])
                                status = "Warning"


        if(with_blockchain > 0):
            automaticAnalysis(treatid, message, "")

        timestamp = datetime.now()

        db.treatment_1.update_one( {'treatid':treatid}, {"$set": {"analysis": {
                            "description": message,
                            # "hash": strHash,
                            "timestamp": timestamp
                        }}})

        return jsonify(status=status, message=message, data={'treatid':treatid, 'patid':patid, 'docid':docid})

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/meetingResult",methods=['POST'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def meetingResult():
    try:
        print(request.form)
        json_data = request.form

        treatid = json_data['treatid']
        docid = json_data['docid']
        patid = json_data['patid']
        result = json_data['result']

        message = ""
        if result == True or result.lower() == "true":
            message = "Meeting requested by doctor " + docid + " for patient " + patid
        else:
            message = "Doctor " + docid + " refused the meeting for patient " + patid

        timestamp = datetime.now()

        newConsultation(treatid, message, "")
        timestamp = datetime.now()

        db.treatment.update_one( {'treatid':treatid}, {"$set": {"meetingResult": {
                            "description": message,
                            # "hash": strHash,
                            "timestamp": timestamp
                        }}})

        return jsonify(status='OK',message="ret_val")

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


def newHospitalization(treatid, patid, docid, icd, atc, date, description, url):
    user = w3.eth.contract(address=contract_address, abi=abi)
    try:

        nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)
        tx = user.functions.newHospitalization(treatid, patid, docid, icd, atc, date, description, url).buildTransaction({
            'from': w3.eth.defaultAccount,
            'chainId': chainId,
            'gas': 2000000,
            'gasPrice': w3.toWei('1', 'gwei'),
            'nonce': nonce
        })

        saveTransaction(treatid, "hash", tx)

    except Exception as e:
        print(str(e))


def automaticAnalysis(treatid, description, url):

    user = w3.eth.contract(address=contract_address, abi=abi)

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.automaticAnalysis(treatid, description, url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 2000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce
    })

    saveTransaction(treatid, "analysis.hash", tx)


def newConsultation(treatid, description, url):

    user = w3.eth.contract(address=contract_address, abi=abi)

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.newConsultation(treatid, description, url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 2000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce
    })

    saveTransaction(treatid, "meetingResult.hash", tx)


def fileUpload(username, treatid, file):

    filename = secure_filename(file.filename)
    # print(filename)
    #make a POST request
    import requests
    try:
        url = 'http://christiansicari.it:8015/uploadFile'
        data = {'username':username, 'treatid':treatid}
        res = requests.post(url, data=data, files= {'file': (filename, file)})
    
    except Exception as e:
        print(str(e))

    url = res.json()['url']
    print("url", url)

    return url


def fileUploadIpfs(file):

    filename = secure_filename(file.filename)
    basedir = os.path.abspath(os.path.dirname(__file__))
    file.save(os.path.join(basedir, filename))

    res = ipfs.add(filename)
    print(res, file=sys.stdout)

    fileHash = res['Hash']
    ipfsPath = "https://ipfs.infura.io/ipfs/"
    url = ipfsPath + fileHash

    return url

def runMatchMaker(patid, icd, atc):
    return patient.getDiagnosis(patid, icd, atc)

def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt


def saveTransaction(treatid, document, tx):
    print('Saving transaction')
    try:
        status = 'Pending'
        timestamp = datetime.now()

        db.pending_transactions.insert_one({
            'treatid':treatid, 'document': document, 'tx':tx,'timestamp':timestamp,'status':status
            })

    except Exception as e:
        print(str(e))


if __name__=='__main__':
    app.run(host="0.0.0.0", port=4000)
