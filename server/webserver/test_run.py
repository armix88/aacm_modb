
import glob
import json
from web3 import Web3
import threading

import requests
import time
from werkzeug.utils import secure_filename

countId = 0
filename = "1MB.jpg"
file = open("/Users/armi/Downloads/4_files/" + filename, 'rb')

HTTPProvider = "https://ropsten.infura.io/v3/94b630ca88fb4376a4adce76becc1b73"
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))

def fileUpload(ethAccount, privateKey, file, with_nc_upload, with_blockchain):
    # print(file)
    # filename = secure_filename(file.filename)
    try:
        global countId
        countId += 1
        url = 'http://90.147.188.235:4000/addTreatment'
        data = {
                "patid": countId
                , "docid": "b"
                , "date": "2019-10-14"
                , "icd": "1 A109, 2 B204"
                , "atc": "3 C709, 4 E1.05"
                , "description":"description"
                , "ethAccount": ethAccount
                , "privateKey": privateKey 
                # , "with_ipfs_upload": 0
                , "with_nc_upload": with_nc_upload
                , "with_blockchain": with_blockchain
                
                }

        res = requests.post(url, data=data, files= {'file': (filename, file)}, timeout=600)
        # print(countId)
    
        print("res", res.json())
        # print("res", res.json()['status'])

    except Exception as e:
        print(str(e))
        return ""

    return 0


def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt


def sendTransaction(file, i = 0, n = 10, with_nc_upload = 0, with_blockchain = 0):
    try:
        with open(file) as keyfile:

            encrypted = keyfile.read()
            ethAccount = json.loads(encrypted)["address"]
            print(ethAccount)
            privateKey = w3.eth.account.decrypt(encrypted, 'test')
            privateKey = privateKey.hex() 

            while i < n:
                fileUpload(Web3.toChecksumAddress(ethAccount), privateKey, file, with_nc_upload, with_blockchain)
                i += 1

    except Exception as e:
        print(str(e))


if __name__=='__main__':

    path = "/Users/armi/Library/Ethereum/testnet/keystore/**/*"

    files = [f for f in glob.glob(path, recursive=True)]
    ### TEST 1 ### 100 trans, no BC, no File
    trans = 250
    c = 100
    for j in range(c):
        print("Account", j)
        threading.Thread(target = sendTransaction, args = (files[j], int((j)*(trans/c)), int((j+1)*(trans/c)), 0, 1)).start()
        time.sleep(1)
        # print(j, int((j)*(trans/c)), int((j+1)*(trans/c)))
    
    print("Account #", c, "Transactions", trans, "Trans/account", int(trans/c))