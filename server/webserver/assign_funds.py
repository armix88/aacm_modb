import requests
import time
from werkzeug.utils import secure_filename

i = 0
# filename = "1MB.jpg"
# file = open("/Users/armi/Downloads/4_files/" + filename, 'rb')

def fileUpload(ethAccount, privateKey, file):
    # print(file)
    # filename = secure_filename(file.filename)
    try:
        global i
        i += 1
        url = 'http://90.147.188.235:4000/addTreatment'
        data = {
                "patid": i
                , "docid": "b"
                , "date": "2019-10-14"
                , "icd": "1 A109, 2 B204"
                , "atc": "3 C709, 4 E1.05"
                , "description":"description"
                , "ethAccount": ethAccount
                , "privateKey": privateKey
                # , "with_ipfs_upload": 0
                , "with_nc_upload": 1
                , "with_blockchain": 0

                }

        res = requests.post(url, data=data, files= {'file': (filename, file)})

    except Exception as e:
        print(str(e))
        return ""

    print("res", res.json())
    # url = res.json()['url']
    # print("url", url)

    return 0


def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt



import glob
import json
from web3 import Web3
import sys

HTTPProvider = "https://ropsten.infura.io/v3/94b630ca88fb4376a4adce76becc1b73"
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))
if __name__=='__main__':

    path = "/Users/armi/Library/Ethereum/testnet/keystore/**/*"

    print(sys.argv[1:])

    j = int(sys.argv[1])
    n = int(sys.argv[2])

    i = -1

    ethAccountMaster = sys.argv[3]
    privateKey = sys.argv[4]
    files = [f for f in glob.glob(path, recursive=True)]

    for f in files:
        with open(f) as keyfile:
          i += 1
          if(i < j):
            continue
          else:
            print(j)
            j += 1
            if j > n:
                sys.exit()

            encrypted = keyfile.read()
            ethAccount = json.loads(encrypted)["address"]
            print(ethAccount)
            # privateKey = w3.eth.account.decrypt(encrypted, 'test')
            # privateKey = privateKey.hex()


            signed_txn = w3.eth.account.signTransaction(dict(
                nonce=w3.eth.getTransactionCount(ethAccountMaster),
                gasPrice = w3.eth.gasPrice,
                gas = 100000,
                to=w3.toChecksumAddress(ethAccount),
                value=w3.toWei(0.1,'ether')
                ),
                privateKey)

            tx_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
            waitForReceipt(tx_hash, 240, 4)
            # fileUpload(Web3.toChecksumAddress(ethAccount), privateKey, file)
