import json
# from flask import Flask, Response, request, jsonify
from flask import Flask,render_template,jsonify,json,request
from flask_cors import CORS, cross_origin
from web3 import Web3
import base64
import sys
import os
from werkzeug.utils import secure_filename
import pymongo
import hashlib
import time

from datetime import datetime
import patient


# Get stored abi and contract_address
with open("smart_contract/build/contracts/Hospital.json", 'r') as f:
    datastore = json.load(f)
    abi = datastore["abi"]
    # bytecode = datastore['bin']

threshold = 0
# Initializing flask app
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/getDiagnosis": {"origins": "*"}
, r"/addPatient": {"origins": "*"}
, r"/getPatientList": {"origins": "*"}
, r"/getDoctorList": {"origins": "*"}
, r"/getTreatments": {"origins": "*"}
, r"/getIcdList": {"origins": "*"}
, r"/getAtcList": {"origins": "*"}
, r"/meetingResult": {"origins": "*"}
, r"/addTreatment": {"origins": "*"}})

import ipfsApi
ipfs = ipfsApi.Client(host='https://ipfs.infura.io', port=5001)

# Read the configuration from the file config.json
def read_config():
    with open('config.json') as file:
        config = json.load(file)
        return config

config = read_config()

# privateKey = base64.b64decode(config["p_key"]).decode()
chainId = config["chainId"]
contract_address = config["contract_address"]
# defaultAccount = config["defaultAccount"]
HTTPProvider = config['HTTPProvider']
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))
# w3.eth.defaultAccount = defaultAccount

client = pymongo.MongoClient(os.environ["DB_HOST_PORT"])
db = client.patient


@app.route('/')
def showList():
    return render_template('list.html')



@app.route("/getPatientList",methods=['GET'])
def getPatientList():
    try:
        patList = patient.getPatientList()

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))

    return jsonify({'data': patList}), 200


@app.route("/getDoctorList",methods=['GET'])
def getDoctorList():
    try:
        docList = patient.getDoctorList()

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))

    return jsonify({'data': docList}), 200


@app.route("/getIcdList",methods=['GET'])
def getIcdList():
    try:
        icdList = patient.getIcdList()

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))

    return jsonify({'data': icdList}), 200


@app.route("/getAtcList",methods=['GET'])
def getAtcList():
    try:
        atcList = patient.getAtcList()

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))

    return jsonify({'data': atcList}), 200



@app.route("/getDiagnosis",methods=['POST'])
def getDiagnosis():
    try:
        icd = ""
        json_data = request.json['info']
        patid = json_data['patid']
        if('icd' in json_data):
            icd = json_data['icd']

        patient.getDiagnosis(patid, icd)

        return jsonify(status='OK',message='inserted successfully')

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/addPatient",methods=['POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def addPatient():

    try:
        json_data = request.form

        name = json_data['name']
        surname = json_data['surname']
        sex = json_data['sex']
        dob = json_data['dob']
        city = json_data['city']

        patid = name + surname + sex + dob + city
        patid = hashlib.sha1(patid.encode()).hexdigest()

        db.patient.insert_one({
            'patid':patid,'name':name,'surname':surname,'sex':sex,'dob':dob,'city':city
            })
        
        return jsonify(status='OK',message='inserted successfully')

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/addTreatment",methods=['POST'])
def addTreatment():
    try:
        json_data = request.form
        username = "root"

        patid = json_data['patid']
        docid = json_data['docid']
        date = json_data['date']
        icd = json_data['icd']
        atc = json_data['atc']
        description = json_data['description']

        # ethAccount = json_data['ethAccount']
        # privateKey = json_data['privateKey']

        # with_nc_upload = int(json_data['with_nc_upload'])
        # with_blockchain = int(json_data['with_blockchain'])

        ethAccount = config["defaultAccount"]
        privateKey = config['p_key']

        with_nc_upload = 1
        with_blockchain = 1

        status = 'In progress'

        treatid = patid + docid + date + icd
        treatid = hashlib.sha1(treatid.encode()).hexdigest()

        if(with_nc_upload > 0):
            if 'file' in request.files:
                file = request.files['file']

        url = ""
        # Uncomment below for IPFS upload (takes some time while testing)
        if(with_nc_upload > 0):
            url = fileUploadIpfs(file)

        # check if the post request has the file part
        # if 'file' not in request.files:
        #     print("No file found in message", file=sys.stdout)
        #     return jsonify({"data": "No file found in message"}), 200

        # file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        # if file.filename == '':
        #     print("Unable to read file", file=sys.stdout)
        #     return jsonify({"data": "Unable to read file"}), 200

        # Uncomment below for IPFS upload (takes some time while testing)
        # url = fileUpload(username, treatid, file)

        icdList = []
        icdListTmp = icd.split(',')
        for element in icdListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            icdDict = {}
            icdDict['icd'] = first
            icdDict['icd_name'] = remainder
            icdList.append(icdDict)

        atcList = []
        atcListTmp = atc.split(',')
        for element in atcListTmp:
            first, remainder = element.split(' ', maxsplit=1)
            atcDict = {}
            atcDict['atc'] = first
            atcDict['atc_name'] = remainder
            atcList.append(atcDict)

        timestamp = datetime.now()

        db.treatment.insert_one({
            'treatid':treatid,'patid':patid,'docid':docid,'icd':icdList,'atc':atcList,'date':date,'description':description,'documentation':url,'timestamp':timestamp,'status':status
            })


        strHash = ""

        if(with_blockchain > 0): 
            strHash = newHospitalization(ethAccount, privateKey, treatid, patid, docid, icd, atc, date, description, url)

        db.treatment.find_one_and_update({'treatid':treatid}, {"$set": { 
                                        "hash": strHash}})

        res_patient, res_deceased = runAlgoritmo(patid, icdList, atcList)

        message = "Nothing found for this treatment"
        status = "Success"

        if len(res_deceased) > 0:
            message = "Warning! A similar treatment lead to death in " + str(len(res_deceased)) + " case(s).\nA meeting with the department is recommended!"
            status = "Warning"
        else:
            for patient in res_patient:
                for icdTemp in icdList:
                    for atcTemp in atcList:
                        if patient['_id']['icd'] == icdTemp['icd'] and patient['_id']['atc'] == atcTemp['atc']:
                            if patient['effectiveness'] > threshold:
                                message = "Treatment is ok"
                                status = "Success"
                            else:
                                message = "There is a problem with the treatment suggested for patid".join([patid, " and atc", atc, " for icd", icd, "\nA meeting with the department is recommended!"])
                                status = "Warning"

        strHash = ""
        if(with_blockchain > 0): 
            strHash = automaticAnalysis(ethAccount, privateKey, treatid, message, "")
        timestamp = datetime.now()

        db.treatment.update_one( {'treatid':treatid}, {"$set": {"analysis": {
                            "description": message,
                            "hash": strHash,
                            "timestamp": timestamp
                        }}})

        return jsonify(status=status, message=message, data={'treatid':treatid, 'patid':patid, 'docid':docid})

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))


@app.route("/meetingResult",methods=['POST'])
def meetingResult():
    try:
        print(request.form)
        json_data = request.form

        treatid = json_data['treatid']
        docid = json_data['docid']
        patid = json_data['patid']
        result = json_data['result']
        
        message = ""
        if result == True or result.lower() == "true":
            message = "Meeting requested by doctor " + docid + " for patient " + patid
        else:
            message = "Doctor " + docid + " refused the meeting for patient " + patid

        timestamp = datetime.now()

        strHash = newConsultation(treatid, message, "")
        timestamp = datetime.now()

        db.treatment.update_one( {'treatid':treatid}, {"$set": {"meetingResult": {
                            "description": message,
                            "hash": strHash,
                            "timestamp": timestamp
                        }}})
        
        return jsonify(status='OK',message="ret_val")

    except Exception as e:
        return jsonify(status='ERROR',message=str(e))

@app.route('/')
# def showList():
#     return render_template('list.html')

@app.route('/getPatient',methods=['GET'])
def getPatient():
    
    # idContract = None
    # if 'idContract' in request.args:
    #         idContract = request.args['idContract']
    
    try:
        # json_data = request.json['info']
        # patid = json_data['patid']
        # query = { "patid":  patid}

        pipeline = [
                # {'$match': query }
                # ,{'$unwind': '$diseases'}   
                # ,{'$unwind': '$treatment'}   
                ]

        patients = db.patient.aggregate(pipeline, allowDiskUse=True)

        patientList = []
        for patient in patients:

            patientItem = {
                    'patid':patient['patid'],
                    'name':patient['name'],
                    'surname':patient['surname']
                    # ,
                    # 'diseases':patient['diseases']['icd'] + ' - ' + patient['diseases']['icd_name'],
                    # 'diseases_date':patient['diseases']['date']
                    }
                    
            # if('treatment' in patient):
            #     treatments = patient['treatment']
                # treatment_str = ""
                # for treatment in treatments:
                
                #     if('atc' in treatment):
                #         treatment_str += treatment['atc']
                #     if('icd' in treatment):
                #         treatment_str += ' - '.join(treatment['icd'])
                #     if('effectiveness' in treatment):
                #         treatment_str += ' - ' + str(treatment['effectiveness']) + '\n'
                #     if('date' in treatment):
                #         treatment_date = str(treatment['date'])
                #         patientItem['treatment_date'] = treatment_date
               
                # patientItem['treatment'] = treatment_str.strip()
                # patientItem['treatment'] = treatments

            patientList.append(patientItem)

        return json.dumps(patientList)
    except Exception as e:
        return str(e)


@app.route("/getTreatments",methods=['GET'])
def getTreatments():

    patients = patient.getTreatments()
    # print(patients)
    """
    patientList = []
    for patient in patients:
        patientItem = {
                'patid':patient['patid'],
                'name':patient['name'],
                'surname':patient['surname']
                # ,
                # 'diseases':patient['diseases']['icd'] + ' - ' + patient['diseases']['icd_name'],
                # 'diseases_date':patient['diseases']['date']
                }
        # if('treatment' in patient):
        #     treatments = patient['treatment']
            # treatment_str = ""
            # for treatment in treatments:
            
            #     if('atc' in treatment):
            #         treatment_str += treatment['atc']
            #     if('icd' in treatment):
            #         treatment_str += ' - '.join(treatment['icd'])
            #     if('effectiveness' in treatment):
            #         treatment_str += ' - ' + str(treatment['effectiveness']) + '\n'
            #     if('date' in treatment):
            #         treatment_date = str(treatment['date'])
            #         patientItem['treatment_date'] = treatment_date
            
            # patientItem['treatment'] = treatment_str.strip()
            # patientItem['treatment'] = treatments

        patientList.append(patientItem)
    # except Exception as e:
    #     return str(e)
    """

    # a = json.dumps(patients)
    # return json.dumps(patients)
    return jsonify({'data': patients}), 200

""""""

@app.route('/getProposals', methods=['GET'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def getProposals():

    user = w3.eth.contract(address=contract_address, abi=abi)
    body = request.get_json()

    user_data = user.functions.getProposals().call()
    #user_data = list(set(user_data))
    user_data_hex = list()
    for user_data_byte in user_data:
        user_data_hex.append(user_data_byte.hex())

    return jsonify({"data": user_data_hex}), 200


@app.route('/readProposal', methods=['GET'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def readProposal():
    idContract = None
    if 'idContract' in request.args:
            idContract = request.args['idContract']

    user = w3.eth.contract(address=contract_address, abi=abi)
    body = request.get_json()

    user_data = user.functions.readProposal(idContract).call()
    return jsonify({"data": user_data}), 200


@app.route('/submitProposal', methods=['GET', 'POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def submitProposal():

    idContract = None
    if 'idContract' in request.args:
            idContract = request.args['idContract']
    idUser1 = None
    if 'idUser1' in request.args:
            idUser1 = request.args['idUser1']
    idUser2 = None
    if 'idUser2' in request.args:
            idUser2 = request.args['idUser2']
    url = None
    if 'url' in request.args:
            url = request.args['url']

    user = w3.eth.contract(address=contract_address, abi=abi)
    body = request.get_json()

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.submitProposal(idContract, Web3.toChecksumAddress(idUser1), Web3.toChecksumAddress(idUser2), url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 4000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce,
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)
    #user_data = w3.eth.waitForTransactionReceipt(tx_hash)

    print("Hash returned ", file=sys.stdout)
    print(user_data['transactionHash'].hex(), file=sys.stdout)

    return jsonify({"data": user_data['transactionHash'].hex()}), 200


@app.route('/submitProposalWithAttachment', methods=['POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def submitProposalWithAttachment():

    print(request.args, file=sys.stdout)
    print(request.form, file=sys.stdout)
    print(request.files, file=sys.stdout)
    """
    idContract = None
    if 'idContract' in request.args:
            idContract = request.args['idContract']
    idUser1 = None
    if 'idUser1' in request.args:
            idUser1 = request.args['idUser1']
    idUser2 = None
    if 'idUser2' in request.args:
            idUser2 = request.args['idUser2']

    """

    idContract = None
    if 'idContract' in request.form:
            idContract = request.form['idContract']
    idUser1 = None
    if 'idUser1' in request.form:
            idUser1 = request.form['idUser1']
    idUser2 = None
    if 'idUser2' in request.form:
            idUser2 = request.form['idUser2']
    url = None
    if 'url' in request.form:
            url = request.form['url']


    # check if the post request has the file part
    if 'file' not in request.files:
        print("Error1", file=sys.stdout)
        return jsonify({"data": "Error1"}), 200
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        print("Error2", file=sys.stdout)
        return jsonify({"data": "Error2"}), 200

    filename = secure_filename(file.filename)

    basedir = os.path.abspath(os.path.dirname(__file__))

    file.save(os.path.join(basedir, filename))

    import ipfsApi
    ipfs = ipfsApi.Client(host='https://ipfs.infura.io', port=5001)

    #res = api.add('route.txt')
    res = api.add(filename)

    print(res, file=sys.stdout)

    fileHash = res['Hash']
    ipfsPath = "https://ipfs.infura.io/ipfs/"
    url = ipfsPath + fileHash

    print("idContract " + idContract, file=sys.stdout)
    print("idUser1 " + idUser1, file=sys.stdout)
    print("idUser2 " + idUser2, file=sys.stdout)
    print("url " + url, file=sys.stdout)

    user = w3.eth.contract(address=contract_address, abi=abi)
    body = request.get_json()

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.submitProposal(idContract, Web3.toChecksumAddress(idUser1), Web3.toChecksumAddress(idUser2), url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 4000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce,
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)
    #user_data = w3.eth.waitForTransactionReceipt(tx_hash)

    print("Hash returned ", file=sys.stdout)
    print(user_data['transactionHash'].hex(), file=sys.stdout)
    return jsonify({"data": user_data['transactionHash'].hex()}), 200

@app.route('/submitReviewUser', methods=['POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def submitReviewUser():

    idContract = None
    if 'idContract' in request.args:
            idContract = request.args['idContract']
    url = None
    if 'url' in request.args:
            url = request.args['url']

    user = w3.eth.contract(address=contract_address, abi=abi)
    body = request.get_json()

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.submitReviewUser(idContract, url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 4000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce,
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)
    #user_data = w3.eth.waitForTransactionReceipt(tx_hash)

    print("Hash returned ", file=sys.stdout)
    print(user_data['transactionHash'].hex(), file=sys.stdout)

    return jsonify({"data": user_data['transactionHash'].hex()}), 200

def newHospitalization(ethAccount, privateKey, treatid, patid, docid, icd, atc, date, description, url):

    user = w3.eth.contract(address=contract_address, abi=abi)

    nonce = w3.eth.getTransactionCount(ethAccount)

    tx = user.functions.newHospitalization(treatid, patid, docid, icd, atc, date, description, url).buildTransaction({
        'from': ethAccount,
        'chainId': chainId,
        'gas': 2000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)

    return user_data['transactionHash'].hex()

def automaticAnalysis(ethAccount, privateKey, treatid, description, url):

    user = w3.eth.contract(address=contract_address, abi=abi)
    # body = request.get_json()

    nonce = w3.eth.getTransactionCount(ethAccount)

    tx = user.functions.automaticAnalysis(treatid, description, url).buildTransaction({
        'from': ethAccount,
        'chainId': chainId,
        'gas': 2000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)

    return user_data['transactionHash'].hex()


def newConsultation(treatid, description, url):

    user = w3.eth.contract(address=contract_address, abi=abi)
    # body = request.get_json()

    nonce = w3.eth.getTransactionCount(w3.eth.defaultAccount)

    tx = user.functions.newConsultation(treatid, description, url).buildTransaction({
        'from': w3.eth.defaultAccount,
        'chainId': chainId,
        'gas': 2000000,
        'gasPrice': w3.toWei('1', 'gwei'),
        'nonce': nonce
    })

    signed = w3.eth.account.signTransaction(tx, privateKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)

    # Wait for transaction to be mined...
    user_data = waitForReceipt(tx_hash, 240, 4)

    # print("Hash returned ", file=sys.stdout)
    # print(user_data['transactionHash'].hex(), file=sys.stdout)
    return user_data['transactionHash'].hex()


def fileUpload(username, treatid, file):

    filename = secure_filename(file.filename)
    # print(filename)
    #make a POST request
    import requests
    try:
        url = 'http://christiansicari.it:8015/uploadFile'
        data = {'username':username, 'treatid':treatid}
        res = requests.post(url, data=data, files= {'file': (filename, file)})
    
    except Exception as e:
        print(str(e))

    print("res", res.json())
    url = res.json()['url']
    print("url", url)

    return url



def fileUploadIpfs(file):

    filename = secure_filename(file.filename)
    basedir = os.path.abspath(os.path.dirname(__file__))
    file.save(os.path.join(basedir, filename))

    res = ipfs.add(filename)
    print(res, file=sys.stdout)

    fileHash = res['Hash']
    ipfsPath = "https://ipfs.infura.io/ipfs/"
    url = ipfsPath + fileHash
    
    return url

def runAlgoritmo(patid, icd, atc):
    return patient.getDiagnosis(patid, icd, atc)

def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt

if __name__=='__main__':
    app.run(host="0.0.0.0", port=8001)
