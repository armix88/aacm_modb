from datetime import datetime
import pymongo
from bson.json_util import dumps
import json
import os
import multiprocessing
from multiprocessing_mapreduce import SimpleMapReduce
import operator

client = pymongo.MongoClient(os.environ["DB_HOST_PORT"])
db = client.patient

def getPatients(collection, patid, icd, atc):

   query = { "patid": patid }
   queryList = []

   for icdTemp in icd:
      for atcTemp in atc:
         queryList.append({'icd.icd': icdTemp['icd'], 'atc.atc': atcTemp['atc']})

   query_icd_atc = {"$or": queryList}

   pipeline = [ 
   { 
      '$match': query
   },
   { 
      '$unwind': { 
         'path':'$icd'
      }
   },
   { 
      '$unwind': { 
         'path':"$atc"
      }
   },
   { 
      '$match': query_icd_atc
   },
   { 
      '$lookup':{ 
         'from':'indications',
         'localField':'icd.icd',
         'foreignField':'icd',
         'as':'test'
      }
   },
   { 
      '$unwind': { 
         'path':"$test"
      }
   },
   { 
      '$project': { 
         'view':{ 
            "$eq":[ 
               '$atc.atc',
               '$test.atc'
            ]
         },
         'patid':'$patid',
         'atc':'$test.atc',
         'atc_name':'$test.atc_name',
         'icd':'$test.icd',
         'icd_name':'$test.icd_name',
         'effectiveness':'$test.effectiveness'
      }
   },
   { 
      '$match': { 
         'view':{ 
            "$eq": True
         }
      }
   },
   { 
      '$group': { 
         '_id':{ 
            'patid':'$_id.patid',
            'atc':'$atc',
            'icd':'$icd'
         },
         'effectiveness':{ 
            '$sum':'$effectiveness'
         }
      }
   }
]

   # print("pipeline", pipeline)
   res = collection.aggregate(pipeline)

   result = json.loads(dumps(res))
   # print("result", result)
   return result


def getPatientsDeceased(collection, icd, atc):

   queryList = []
   for icdTemp in icd:
      for atcTemp in atc:
         queryList.append({'icd.icd': icdTemp['icd'], 'atc.atc': atcTemp['atc']})

   query_icd_atc = {"$or": queryList}
   query_icd_atc['status'] = "Deceased"
   pipeline = [
   { 
      '$unwind': { 
         'path':'$icd'
      }
   },
   { 
      '$unwind': { 
         'path':"$atc"
      }
   },
   { 
      '$match': query_icd_atc
   }
]

   # print("pipeline", pipeline)
   res = collection.aggregate(pipeline)

   result = json.loads(dumps(res))
   # print("result", result)
   return result


def getPatList(collection):

    pipeline = [ 
   { 
      '$project': { 
         'patid':'$patid',
         'name':'$name',
         'surname':'$surname'
      }
   }
   ]

   #  print("pipeline", pipeline)
    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result


def getDocList(collection):

    pipeline = [ 
   { 
      '$project': { 
         'docid':'$docid',
         'name':'$name',
         'surname':'$surname'
      }
   }
   ]
   #  print("pipeline", pipeline)
    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result



def getIcdAll(collection):

    pipeline = [ 
   { 
      '$unwind': { 
         'path':'$diseases'
      }
   },
   {
      '$project': { 
         'code': '$diseases.code',
         'name': '$diseases.name'
      }
   }
   ]

    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result


def getAtcAll(collection):

    pipeline = [ 
   { 
      '$project': { 
         'code':'$code',
         'name':'$displayName'
      }
   }
   ]
    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result


def getPatientsAll(collection):

    pipeline = [ 
   { 
      "$lookup": { 
         'from':'patient',
         'localField':'patid',
         'foreignField':'patid',
         'as':'pat'
      }
   },
   { 
      "$unwind": { 
         'path':"$pat"
      }
   },
   { 
      "$lookup": { 
         'from':'doctor',
         'localField':'docid',
         'foreignField':'docid',
         'as':'doc'
      }
   },
   { 
      "$unwind": { 
         'path':"$doc"
      }
   },
   { 
      "$project": { 
         'doc':'$doc.surname',
         'name':'$pat.name',
         'surname':'$pat.surname',
         'icd':'$icd',
         'atc':'$atc',
         'description':'$description',
         'documentation':'$documentation',
         'status':'$status',
         'analysis':'$analysis',
         'meetingResult':'$meetingResult',
         'hash':'$hash'
      }
   }
]

   #  print("pipeline", pipeline)
    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result


def getPendingTrans(collection):

    pipeline = [ 
      { 
         '$match': { 
            'status':'Pending'
         }
      },
      { 
         '$sort': { 
            'timestamp':1
         }
      },
      { 
         '$limit': 1
      }
   ]

   #  print("pipeline", pipeline)
    res = collection.aggregate(pipeline)

    result = json.loads(dumps(res))
   #  print("result", result)
    return result
    

def get_patients(result):
    """Read a file and return a sequence of
    (word, occurences) values.
    """
    output = []

    temp = []

    temp.append(result['_id']['patid'])
    temp.append(result['_id']['atc'])
    temp.append(result['effectiveness'])
    output.append(tuple(temp))
    
    return output


def count_words(item):
    """Convert the partitioned data for a word to a
    tuple containing the word and the number of occurences.
    """
    patid, tpl = item

    return (patid, max(eff for _, eff in tpl))

def procedural(result):
     temp1 = dict()
     temp2 = dict()
     final = dict()
     for element in result:
          patid = element['_id']['patid']
          atc = element['_id']['atc']
          icd = element['icd']
          effectiveness = element['effectiveness']

          key = patid + "***" + str(effectiveness)
          if patid in temp2:
               if effectiveness > temp2[patid]:
                    temp2[patid] = effectiveness
          else:
               temp2[patid] = effectiveness

          if key not in temp1:
               temp1[key] = (atc, icd)
     
     for patid in temp2:
          key = patid + "***" + str(temp2[patid])
          final[key] = temp1[key]

     return final

def getTreatments():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["treatment"]
    result_patients = getPatientsAll(collection)

    return result_patients

def getPatientList():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["patient"]
    result_patients = getPatList(collection)

    return result_patients


def getDoctorList():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["doctor"]
    result_patients = getDocList(collection)

    return result_patients

def getIcdList():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["icd10"]
    result_patients = getIcdAll(collection)

    return result_patients

def getAtcList():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["atc"]
    result_patients = getAtcAll(collection)

    return result_patients

def getPendingTransactions():

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["pending_transactions"]
    result = getPendingTrans(collection)

    return result

def getDiagnosis(patid, icd, atc):

   #  client = pymongo.MongoClient()
   #  db = client["patient"]
    collection = db["treatment"]
    result_patients = getPatients(collection, patid, icd, atc)

    result_deceased = getPatientsDeceased(collection, icd, atc)
    return result_patients, result_deceased
    """

    """
    res = procedural(result)

    # mapper = SimpleMapReduce(get_patients, count_words)
    # word_counts = mapper(result)
    # word_counts.sort(key=operator.itemgetter(1))
    # word_counts.reverse()

    for key in res:
        patid, effectiveness = key.split("***")
        atc, icd = res[key]

        collection.find_one_and_update({"patid": patid}, 
                                    {"$push": { "treatment": {
                                        "atc": atc
                                        , "icd": icd
                                        , "effectiveness": effectiveness
                                        ,"date": datetime.now()
                                    }}})
