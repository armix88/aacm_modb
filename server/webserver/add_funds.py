import requests
import time
from werkzeug.utils import secure_filename

i = 0
# filename = "1MB.jpg"
# file = open("/Users/armi/Downloads/4_files/" + filename, 'rb')

def fileUpload(ethAccount):
    # print(file)
    # filename = secure_filename(file.filename)
    try:
        global i
        i += 1
        url = 'https://faucet.ropsten.be/donate/' + ethAccount
        # data = {
        #         "patid": i
        #         , "docid": "b"
        #         , "date": "2019-10-14"
        #         , "icd": "1 A109, 2 B204"
        #         , "atc": "3 C709, 4 E1.05"
        #         , "description":"description"
        #         , "ethAccount": ethAccount
        #         , "privateKey": privateKey
        #         # , "with_ipfs_upload": 0
        #         , "with_nc_upload": 1
        #         , "with_blockchain": 0

        #         }

        # res = requests.post(url, data=data, files= {'file': (filename, file)})
        res = requests.post(url)

    except Exception as e:
        print(str(e))
        return ""

    print("res", res)
    # url = res.json()['url']
    # print("url", url)

    return 0


def waitForReceipt(tx_hash, timeout = 60, interval = 2):
  t0 = time.time()
  while(True):
    try:
        receipt = w3.eth.getTransactionReceipt(tx_hash)
        if receipt is not None:
          break
        delta = time.time() - t0
        if(delta > timeout ):
          break
        time.sleep(interval)
    except:
        print("Retry")
  return receipt



import glob
import json
from web3 import Web3

HTTPProvider = "https://ropsten.infura.io/v3/94b630ca88fb4376a4adce76becc1b73"
w3 = Web3(Web3.HTTPProvider(HTTPProvider,request_kwargs={'timeout':6000}))
if __name__=='__main__':

    path = "/Users/armi/Library/Ethereum/testnet/keystore/**/*"

    files = [f for f in glob.glob(path, recursive=True)]
    i = 0
    for f in files:
        with open(f) as keyfile:
            print(i)
            i += 1
            # if i <= 68:
            #     continue

            encrypted = keyfile.read()
            ethAccount = json.loads(encrypted)["address"]
            print(ethAccount)
            # privateKey = w3.eth.account.decrypt(encrypted, 'test')
            # privateKey = privateKey.hex()

            balance = w3.fromWei(w3.eth.getBalance(Web3.toChecksumAddress(ethAccount)), 'ether')
            print(balance)

            # signed_txn = w3.eth.account.signTransaction(dict(
            #     nonce=w3.eth.getTransactionCount('0x2a558A74108f5fb641559b3A295e1Ccc7e22A707'),
            #     gasPrice = w3.eth.gasPrice,
            #     gas = 100000,
            #     to=w3.toChecksumAddress("3978f1d2d4d40bd57b4ee843ef3e4bd6f96e1c57"),
            #     value=w3.toWei(0.001,'ether')
            #     ),
            #     '851ABA4AF636DB9B2DA4DD63AD496E4DC5FBE10F955C1CF20E7E25D200D0D987')

            # tx_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
            # waitForReceipt(tx_hash, 240, 4)
            # fileUpload(Web3.toChecksumAddress(ethAccount))
