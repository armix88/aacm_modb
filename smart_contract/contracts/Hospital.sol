//pragma solidity ^0.4.24;

pragma solidity ^0.5.11;

contract Hospital {

    struct Type {
        string description;
        string name;
        string url;
    }

    struct Treatment {
        string treatId;
        string patId;
        string docId;
        string icd;
        string atc;
        string date;
        string description;
        string url;
        string status;
        mapping (uint256 => Type) typology;
    }

    mapping (string => Treatment) treat;
    string[] treatList;

    function newHospitalization(string memory treatId, string memory patId, string memory docId, string memory icd, string memory atc, string memory date, string memory description, string memory url) public payable {

        treat[treatId].patId = patId;
        treat[treatId].docId = docId;
        treat[treatId].icd = icd;
        treat[treatId].atc = atc;
        treat[treatId].date = date;
        treat[treatId].typology[0].description = description;
        treat[treatId].typology[0].url = url;
        treat[treatId].typology[0].name = "New hospitalization";

        /* addToArray(treatId); */
    }

    function automaticAnalysis(string memory treatId, string memory description, string memory url) public payable {

        require(inArray(treatId), "Contract not found for id");

        treat[treatId].typology[1].name = "Automatic analysis";
        treat[treatId].typology[1].description = description;
        treat[treatId].typology[1].url = url;

    }

     function newConsultation(string memory treatId, string memory description, string memory url) public payable {

        require(inArray(treatId), "Contract not found for id");

        treat[treatId].typology[2].name = "New consultation";
        treat[treatId].typology[2].description = description;
        treat[treatId].typology[2].url = url;

    }

    function drugAdministration(string memory treatId, string memory description, string memory url) public payable {

        require(inArray(treatId), "Contract not found for id");

        treat[treatId].typology[3].name = "Drug administration";
        treat[treatId].typology[3].description = description;
        treat[treatId].typology[3].url = url;
    }


    function closeTreatment(string memory treatId, string memory description, string memory url) public payable {

        require(inArray(treatId), "Contract not found for id");

        treat[treatId].typology[4].name = "Close treatment";
        treat[treatId].typology[4].description = description;
        treat[treatId].typology[4].url = url;
        treat[treatId].status = "Close treatment";
    }

    /*
    pragma experimental ABIEncoderV2;

    function getProposals() view public returns (string [] memory) {
        return treatList;
    }
    function readProposal(bytes32 idContract) view public returns (address, address, string, string, string, string) {
        return (rec[idContract].idUser1, rec[idContract].idUser2
        , rec[idContract].typology[0].url
        , rec[idContract].typology[1].url
        , rec[idContract].typology[2].url
        , rec[idContract].typology[3].url
        );
    }
    */
    function addToArray(string memory idContract) private {

        if (!inArray(idContract)) {
            treatList.push(idContract) -1;
        }
    }

    function inArray(string memory idContract) private view returns (bool) {

        for (uint i = 0; i < treatList.length; i++) {
            // if (treatList[i] == idContract) {
            if (keccak256(bytes(treatList[i])) == keccak256(bytes(idContract))) {
                return true;
            }
        }
        return false;
    }
}
