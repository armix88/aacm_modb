FROM python:3.6
WORKDIR /app
ADD server/webserver/app_jmeter.py /app
ADD server/webserver/patient.py /app
ADD server/webserver/multiprocessing_mapreduce.py /app
ADD server/webserver/config.json /app
ADD smart_contract/build/contracts/ /app/smart_contract/build/contracts
RUN pip3 install flask flask_cors gunicorn web3 ipfs-api pymongo
EXPOSE 4000
CMD ["python3", "app_jmeter.py"]
